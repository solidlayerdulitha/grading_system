<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends Student_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Logger_m');

        $this->logDetail['is_audit']   = '';
        $this->logDetail['old_value']  = '';
        $this->logDetail['new_value']  = '';
        $this->logDetail['controller'] = 'User';
        $this->logDetail['admin_id']   = $this->session->userdata('id');
        $this->logDetail['admin_name'] = $this->session->userdata('name');

    }

    public function index()
    {
        $users = $this->Admin_m->get_by(['deleted' => 0, 'user_type !=' => 1]);
        $roles = $this->Role_m->dropdown_list('name', 'Select Role', ['super_admin' => 0], 'id');

        $this->data['subview'] = $this->_view;
        $this->data['userTypes'] = $roles;
        $this->data['users'] = $users;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    public function login()
    {
        //set log details
        $logDetail                = $this->logDetail;
        $logDetail['description'] = 'Loading login veiw';
        $logDetail['method']      = 'login';

        $this->Admin_m->logged_in() == false || $this->redirectToDashboard();

        $rules = $this->Admin_m->rules;
        $this->form_validation->set_rules($rules);
        // process form
        if ($this->form_validation->run() == true) {
            // echo '1'; die();
            if ($this->Admin_m->login() == true) {
                $logDetail['description']      = 'Login has been successfully';
                $this->logDetail['admin_id']   = $this->session->userdata('id');
                $this->logDetail['admin_name'] = $this->session->userdata('name');
                $this->redirectToDashboard();
            } else {
                // echo $this->Staff_m->hash($this->input->post('pl_password')); die();
                $this->session->set_flashdata('error', 'Login Error! Invalid Email address or password.');
                $logDetail['description'] = 'Error : Invalid Email address or password.';
                redirect('User/login', 'refresh');
            }

        }
        $this->Logger_m->save($logDetail);
        // // load view
        $this->data['subview'] = $this->_view;
        $this->load->view($this->_template . '/_layout_login', $this->data);
    }

    /*
     * View page for the User's account
     *
     * @param  Integer  $id    User's Table ID
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return NO returns
     */
    public function view($id = null)
    {
        //set log details
        // $logDetail = $this->logDetail;
        // $logDetail['description'] = 'Loading user single veiw : ID('.$id.')';
        // $logDetail['method'] = 'view';
        // $this->Logger_m->save($logDetail);

        $this->data['subview'] = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    public function create()
    {

        $rules = $this->Admin_m->userRules;
        $this->form_validation->set_rules($rules);

        $roles = $this->Role_m->dropdown_list('name', 'Select Role', ['super_admin' => 0], 'id');

        if ($this->form_validation->run() == true) {
            $postArray = array('name', 'email', 'password', 'enable', 'user_type');

            $postdata             = $this->Admin_m->array_from_post($postArray);
            $postdata['password'] = $this->Admin_m->hash($postdata['password']);

            $userId = $this->Admin_m->save($postdata);

            if ($userId) {
                $this->session->set_flashdata('success', 'User successfully created');
                redirect('User');
            }

        }

        $this->data['roles']   = $roles;
        $this->data['subview'] = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }
    /*
     * Purpose of this function is to Edit existing User account
     * or Insert new user to the system
     *
     * @param  Integer  $id    User's Table ID
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return No Returns
     */
    public function edit($id = null)
    {
        if ($id != null) {
            $user = $this->Admin_m->get($id);

            if (!empty($user)) {
                $rules = $this->Admin_m->userEditRules;
                $this->form_validation->set_rules($rules);

                $roles = $this->Role_m->dropdown_list('name', 'Select Role', ['super_admin' => 0], 'id');

                if ($this->form_validation->run() == true) {
                    if ($this->form_validation->run() == true) {
                        $postArray = array('name', 'email', 'password', 'enable', 'user_type');

                        $postdata             = $this->Admin_m->array_from_post($postArray);
                        if (!empty($postdata['password'])) {
                            $postdata['password'] = $this->Admin_m->hash($postdata['password']);
                        } else {
                            unset($postdata['password']);
                        }

                        $userId = $this->Admin_m->update_by($postdata, ['id' => $id]);

                        if ($userId) {
                            $this->session->set_flashdata('success', 'User successfully created');
                            redirect('User');
                        }

                    }
                }

                $this->data['user'] = $user;
                $this->data['roles'] = $roles;
            }
        }

        $this->data['subview'] = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    /*
     * Purpose of this function is to change password of user's account
     *
     * @param  Integer  $id    User's Table ID
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return No Returns
     */
    public function changePassword($id = null)
    {
        //set log details
        $logDetail                = $this->logDetail;
        $logDetail['description'] = 'Loading change password veiw';
        $logDetail['method']      = 'changePassword';

        if (!$id) {
            $logDetail['description'] = 'Error : User Id is not available.';
            $this->Logger_m->save($logDetail);
            redirect('User');
        } else {
            $this->data['user'] = $this->Admin_m->get($id);
            $rules              = $this->Admin_m->changePassRules;

            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == true) {
                $data['password'] = $this->Admin_m->hash($this->input->post('password'));
                $this->Admin_m->save($data, $id);
                redirect('User?updated=true');
            }

            $this->Logger_m->save($logDetail);
            $this->data['subview'] = $this->_view;
            $this->load->view($this->_template . '/_layout_main', $this->data);
        }
    }

    /*
     * Purpose of this function is to check duplicate emails in edit user page
     *
     * @param  String  $email    User's email from form
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return true or false
     */
    public function unique_email($email)
    {
        $userId = $this->uri->segment(3);
        $user   = $this->Admin_m->get_by(array('email' => $email));

        if (!empty($user)) {
            foreach ($user as $userData) {
                if ($userId == $userData->id) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return true;
        }
    }

    public function logout()
    {
        //set log details
        $logDetail                = $this->logDetail;
        $logDetail['description'] = 'Log out';
        $logDetail['method']      = 'logout';
        $this->Logger_m->save($logDetail);

        $this->Admin_m->logout();
        redirect('User/login');
    }

    public function redirectToDashboard()
    {
        $studentTypeObj = $this->Admin_type_m->get_by(['machine_name' => 'STUDENT'], true);
        $permissionArray = [
            'Assignment',
            'ClassType',
            'Student',
            'Teacher',
        ];

        if ($this->session->userdata('user_type') == $studentTypeObj->id) {
            $permissionArray = [
                'Profile'
            ];

        }

        $this->session->set_userdata('permission', $permissionArray);
        redirect('Welcome');
    }

    /*
     * Purpose of this function is to return all user types
     *
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return No $userTypes
     */
    private function getUserTypes()
    {
        $userTypes = [];
        foreach ($this->Admin_type_m->get() as $userTypeData) {
            $userTypes[$userTypeData->id] = $userTypeData->admin_type;
        }

        return $userTypes;
    }
}
