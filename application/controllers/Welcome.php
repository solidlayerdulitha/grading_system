<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends Student_Controller
{
	/**
	 * This controller will be shown initially when logged in
	 * @return [type] [description]
	 */
    public function index()
    {
        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }
}