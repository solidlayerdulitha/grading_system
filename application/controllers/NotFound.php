<?php
defined('BASEPATH') or exit('No direct script access allowed');

class NotFound extends Student_Controller
{

    public function index()
    {

        $this->output->set_status_header('404');
        $errorView = $this->_template  . 'NotFound/index';
		$this->data['subview'] = $errorView;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }
}