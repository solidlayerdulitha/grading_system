<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends Student_Controller
{

    /**
     * Get detail results for a student including assignments
     * and results
     */
    public function index()
    {
        $loginId = $this->session->userdata('id');
        // $studentId = 2;

        $studentData = $this->Student_m->get_by(['login_id' => $loginId], true);
        $studentClasses = $this->Student_class_m->get_by(['student_id' => $studentData->id]);

        $classIdsList = [];

        foreach ($studentClasses as $class) {
            if (!array_key_exists($class->id, $classIdsList)) {
                $classIdsList[$class->id] = $class->id;
            }
        }

        $classAssignmentsList = $this->Class_assignment_m->get_in('class_id', $classIdsList);

        $assignmentIdsList = [];

        foreach ($classAssignmentsList as $assignment) {
             if (!array_key_exists($assignment->assignment_id, $assignmentIdsList)) {
                $assignmentIdsList[$assignment->assignment_id] = $assignment->assignment_id;
            }
        }

        $assignments = $this->Assignment_m->get_in('id', $assignmentIdsList);

        $examResults = $this->Results_data_m->get_by(['student_id' => $studentData->id]);

        $this->data['studentData'] = $studentData;
        $this->data['assignments'] = $assignments;
        $this->data['examResults'] = $examResults;
        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    /**
     * This function is used to retrieve data of a specified assignment attempt
     * through AJAX
     * @return JSON
     */
    public function getResultsData()
    {
        if ($this->input->method() == 'post') {
            $resultId = $this->input->post('result_id'); 
            $studentId = $this->input->post('student_id'); 

            $assignmentResult = $this->Results_data_m->get($resultId);
            $examAnswers = $this->Answer_data_m->get_by(['student_id' => $studentId, 'result_data_id' => $resultId]);
            $examQuestions = $this->Assignment_question_m->dropdown_list('question', '', null, 'id', false);

            $structuredResults = $this->Profile_m->structureQuestionsAnswers($examAnswers, $examQuestions, $assignmentResult);
            echo json_encode($structuredResults);
        }
    }
}
