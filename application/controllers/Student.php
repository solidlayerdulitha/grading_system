<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Student extends Admin_Controller
{
    /**
     * Student List
     * @return [type] [description]
     */
    public function index()
    {
        $students     = $this->Student_m->get_by(['status' => 1]);

        $this->data['students'] = $students;
        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    /**
     * Add student to the database and create a login.
     * Default student password will be 'admin'.
     */
    public function add()
    {   
        $classes = $this->Class_m->dropdown_list('class_name', 'Select Class', null, 'id');

        if ($this->input->method() == 'post') {
            $studentTypeObj = $this->Admin_type_m->get_by(['machine_name' => 'STUDENT'], true);

            $postArray = array('student_name', 'email', 'class_id', 'status');
            $postdata = $this->Class_m->array_from_post($postArray);

            $loginSaveArray = [
                'name' => $postdata['student_name'],
                'email' => $postdata['email'],
                'password' => $this->Admin_m->hash('admin'),
                'user_type' => $studentTypeObj->id,
                'enable' => 1
            ];

            $result = $this->Admin_m->save($loginSaveArray);

            $studentSaveArray = [
                'student_name' => $postdata['student_name'],
                'login_id' => $result,
                'email' => $postdata['email']
            ];

            $studentId = $this->Student_m->save($studentSaveArray);

            $classObj = $this->Class_m->get($postdata['class_id']);

            $classSaveArray = [
                'class_id' => $postdata['class_id'],
                'class_name' => $classObj->class_name,
                'student_id' => $studentId,
                'student_name' => $postdata['student_name'],
            ];

            $studentId = $this->Student_class_m->save($classSaveArray);

            $this->session->set_flashdata('success', 'Studemt successfully created');
            redirect('Student');
        }

        $this->data['classes']      = $classes;
        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }
}
