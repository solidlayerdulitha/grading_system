<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ClassType extends Admin_Controller
{

    public function index()
    {
        $classes     = $this->Class_m->get_by(['status' => 1]);

        $this->data['classes'] = $classes;
        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    public function assign($id = null)
    {
        if ($id != null) {
            $classObj = $this->Class_m->get($id);
            if (!empty($classObj)) {
                $assignments = $this->Assignment_m->get();

                if ($this->input->method() == 'post') {
                    $postvalues = $this->input->post();

                    foreach ($postvalues as $postK => $postV) {
                        if (strpos($postK, 'assignment_') !== false) {
                            $explodedPostK = explode("_", $postK);

                            if (!empty($explodedPostK[1])) {
                                $assignmentObj = $this->Assignment_m->get_by(['id' => $explodedPostK[1]], true);

                                $arrayToBeSaved = [
                                    'class_id'        => $id,
                                    'class_name'      => $classObj->class_name,
                                    'assignment_name' => $assignmentObj->assignment_name,
                                    'assignment_id'   => $explodedPostK[1],
                                ];

                                $this->Class_assignment_m->save($arrayToBeSaved);
                            }
                        }
                    }

                    $this->session->set_flashdata('success', 'Assignment successfully updated');
                    redirect('ClassType/view/' . $id);
                }

                $this->data['classAssignments']      = $this->Class_assignment_m->dropdown_list('assignment_id', '', ['class_id' => $id], 'assignment_id', false);

                $this->data['classObj']      = $classObj;
                $this->data['assignments']      = $assignments;
            } else {
                $this->data['error']['message']      = 'This assignment does not exists.';    
            }
        } else {
            $this->data['error']['message']      = 'Please provide an id for the assignment';
        }

        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    public function view($id)
    {
        if ($id != null) {
            $classObj = $this->Class_m->get($id);

            if (!empty($classObj)) {
                $classAssignments = $this->Class_assignment_m->get_by(['class_id' => $classObj->id, 'status' => 1]);

                $this->data['classObj']      = $classObj;
                $this->data['classAssignments']      = $classAssignments;
            }

        }

        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }
}
