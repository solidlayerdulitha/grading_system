<?php
defined('BASEPATH') or exit('No direct script access allowed');

// require(APPPATH.'libraries\RestController.php');
class Api extends Admin_Controller
{
    const API_KEY = 'xQvVXrVcaZh4diC8wQ5s';

    /**
     * Get question details for a given assignment ID [GET]
     * @return JSON
     */
    public function question()
    {
        $header = $this->input->request_headers();

        if (!empty($header["Application-Key"]) && $header['Application-Key'] == self::API_KEY) {
            if (!empty($header["Access-Key"])) {
                $accessKey = $this->validateAccessKey($header["Access-Key"]);

                if ($accessKey['status'] != 'error') {
                    $assignmentId = $this->input->get("assignment");

                    if (!$assignmentId == null && $assignmentId != '') {
                        $questions = $this->Assignment_question_m->get_by(['assignment_id' => $assignmentId]);

                        $this->saveApiLog(['assignment' => $assignmentId], 'Y', '200');
                        $this->output
                           ->set_content_type('application/json')
                           ->set_output(json_encode(['status' => 'success', 'message' => 'successfully retrieved questions for assignment', 'data' => $questions]));
                    } else {
                        $this->saveApiLog([], 'Y', '200', 'No assignment id found');
                        $this->output
                           ->set_content_type('application/json')
                           ->set_output(json_encode(['status' => 'error', 'message' => 'No assignment id found', 'data' => []]));    
                    }
                } else {
                    $this->saveApiLog([], 'N', '403', 'Access key error');
                    $this->output
                       ->set_content_type('application/json')
                       ->set_output(json_encode($accessKey));
                }
            } else {

                $this->saveApiLog([], 'N', '403', 'Access Key could not be found in the request');

                $this->output
                   ->set_content_type('application/json')
                   ->set_output(json_encode(['status' => 'error', 'message' => 'Access Key could not be found in the request', 'data' => []]));    
            }
        } else {
            $this->saveApiLog([], 'N', '403', 'Authentication Key could not be found or invalid Authentication Key provided');
            $this->output
               ->set_content_type('application/json')
               ->set_output(json_encode(['status' => 'error', 'message' => 'Authentication Key could not be found or invalid Authentication Key provided', 'data' => []]));
        }
    }

    /**
     * This function is called by a cron. This will get the results from the external exam tool
     * and structure it as the database table in grading system
     * @return [boolen] [TRUE on success, FALSE on error]
     */
    public function answers()
    {
        $outsideSystemData = [
            'student_list' => [
                '0' => [
                    'student_id' => '1',
                    'assignment' => [
                        '0' => [
                            'assignment_id' => '1',
                            'overall_result' => '74',
                            'time_start' => '2020-07-09 04:42:10',
                            'time_end' => '2020-07-09 06:20:40',
                            'questions' => [
                                '0' => [
                                    'question_id' => '3',
                                    'question' => 'What is computer?',
                                    'answer' => 'an electronic device for storing and processing data, typically in binary form, according to instructions given to it in a variable program.',
                                    'marks' => 'RIGHT'
                                ],
                                '1' => [
                                    'question_id' => '4',
                                    'question' => 'What is programming?',
                                    'answer' => 'Programming is a way to “instruct the computer to perform various tasks”.',
                                    'marks' => 'RIGHT'
                                ],
                            ],
                        ],
                        '1' => [
                            'assignment_id' => '3',
                            'overall_result' => '22',
                            'time_start' => '2020-07-09 07:00:22',
                            'time_end' => '2020-07-09 08:10:30',
                            'questions' => [
                                '0' => [
                                    'question_id' => '5',
                                    'question' => 'What is cyber space?',
                                    'answer' => 'I dont know',
                                    'marks' => 'WRONG'
                                ],
                                '1' => [
                                    'question_id' => '6',
                                    'question' => 'What is Law?',
                                    'answer' => 'a rule defining correct procedure or behaviour in a sport.',
                                    'marks' => 'PARTIAL'
                                ],
                            ],
                        ]
                    ],
                ],
                '1' => [
                    'student_id' => '2',
                    'assignment' => [
                        '0' => [
                            'assignment_id' => '1',
                            'overall_result' => '74',
                            'time_start' => '2020-07-09 08:55:10',
                            'time_end' => '2020-07-09 10:32:40',
                            'questions' => [
                                '0' => [
                                    'question_id' => '3',
                                    'question' => 'What is computer?',
                                    'answer' => 'A device maybe',
                                    'marks' => 'WRONG'
                                ],
                                '1' => [
                                    'question_id' => '4',
                                    'question' => 'What is programming?',
                                    'answer' => 'Programming is a way to “instruct the computer to perform various tasks” - 2.',
                                    'marks' => 'RIGHT'
                                ],
                            ],
                        ],
                        '1' => [
                            'assignment_id' => '2',
                            'overall_result' => '57',
                            'time_start' => '2020-07-09 06:00:22',
                            'time_end' => '2020-07-09 08:59:30',
                            'questions' => [
                                '0' => [
                                    'question_id' => '1',
                                    'question' => 'What is Biology?',
                                    'answer' => 'Biology is the natural science that studies life and living organisms',
                                    'marks' => 'RIGHT'
                                ],
                                '1' => [
                                    'question_id' => '6',
                                    'question' => 'Who are animals?',
                                    'answer' => 'Animals can be insects, mammals, reptiles, fish, and other organisms that are not plants.',
                                    'marks' => 'RIGHT'
                                ],
                            ],
                        ]
                    ],
                ],
            ],

        ];

        $outsideSystemData = json_encode($outsideSystemData);
        $data = json_decode($outsideSystemData);

        //starting a transtaction because this process is critical
        $this->db->trans_start();

        foreach ($data as $studentList) {
            foreach ($studentList as $student) {
                $resultData = [
                    'student_id' => $student->student_id
                ];

                foreach ($student->assignment as $assignment) {
                    $resultData['assignment_id'] = $assignment->assignment_id;
                    $resultData['result'] = $assignment->overall_result;
                    $resultData['time_start'] = $assignment->time_start;
                    $resultData['time_end'] = $assignment->time_end;

                    $resultsObj = $this->Results_data_m->get_by(['student_id' => $student->student_id, 'assignment_id' => $assignment->assignment_id]);
                    $attemptNumber = count($resultsObj) + 1;
                    $resultData['attempt_number'] = $attemptNumber;  

                    $resultDataId = $this->Results_data_m->save($resultData);

                    $questionData = [
                        'student_id' => $student->student_id,
                    ];

                    foreach ($assignment->questions as $question) {
                        $questionData['question_id'] = $question->question_id;
                        $questionData['answer'] = $question->answer;
                        $questionData['result'] = $question->marks;
                        $questionData['result_data_id'] = $resultDataId;
                        $questionData['assignment_id'] = $assignment->assignment_id;

                        $this->Answer_data_m->save($questionData);
                    }
                }
            }
        }

        $this->db->trans_complete();

        $res = true;

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $res = false;
        } else {
             $this->db->trans_commit();
        }

        return $res;
    }

    /**
     * Get all student list with details [GET]
     * @return JSON
     */
    public function student()
    {
        $header = $this->input->request_headers();

        if (!empty($header["Application-Key"]) && $header['Application-Key'] == self::API_KEY) {
            if (!empty($header["Access-Key"])) {
                $accessKey = $this->validateAccessKey($header["Access-Key"]);

                if ($accessKey['status'] != 'error') {
                    $this->saveApiLog([], 'Y', '200');
                    $this->output
                       ->set_content_type('application/json')
                       ->set_output(json_encode(['status' => 'success', 'message' => 'successfully retrieved student list', 'data' => []]));
                } else {
                    $this->saveApiLog([], 'N', '403', 'Access key error');
                    $this->output
                       ->set_content_type('application/json')
                       ->set_output(json_encode($accessKey));
                }
            } else {
                $this->saveApiLog([], 'N', '403', 'Access Key could not be found in the request');
                $this->output
                   ->set_content_type('application/json')
                   ->set_output(json_encode(['status' => 'error', 'message' => 'Access Key could not be found in the request', 'data' => []]));
            }
        } else {
            $this->saveApiLog([], 'N', '403', 'Authentication Key could not be found or invalid Authentication Key provided');
            $this->output
               ->set_content_type('application/json')
               ->set_output(json_encode(['status' => 'error', 'message' => 'Authentication Key could not be found or invalid Authentication Key provided', 'data' => []]));
        }
    }

    /**
     * Get all assignment list of the system [GET]
     * Function status : INCOMPLETE
     * @return JSON
     */
    public function assignment()
    {
        $header = $this->input->request_headers();

        if (!empty($header["Application-Key"]) && $header['Application-Key'] == self::API_KEY) {
            if (!empty($header["Access-Key"])) {
                $accessKey = $this->validateAccessKey($header["Access-Key"]);
                if ($accessKey['status'] != 'error') {
                    $assignments = $this->Assignment_m->get_by(['status' => 1]);

                    $this->saveApiLog([], 'Y', '200');
                    $this->output
                       ->set_content_type('application/json')
                       ->set_output(json_encode(['status' => 'success', 'message' => 'successfully retrieved assignment list', 'data' => $assignments]));
                } else {
                    $this->saveApiLog([], 'N', '403', 'Access key error');
                    $this->output
                       ->set_content_type('application/json')
                       ->set_output(json_encode($accessKey));
                }
            } else {
                $this->saveApiLog([], 'N', '403', 'Access Key could not be found in the request');
                $this->output
                   ->set_content_type('application/json')
                   ->set_output(json_encode(['status' => 'error', 'message' => 'Access Key could not be found in the request', 'data' => []]));
            }
        } else {
            $this->saveApiLog([], 'N', '403', 'Authentication Key could not be found or invalid Authentication Key provided');
            $this->output
               ->set_content_type('application/json')
               ->set_output(json_encode(['status' => 'error', 'message' => 'Authentication Key could not be found or invalid Authentication Key provided.', 'data' => []]));
        }
    }

    /**
     * This function is used to validate the provided Access-Keys validity. If this fails
     * you need to get a new access key using register()
     * @param  String $accessKey 
     * @return Array
     */
    private function validateAccessKey($accessKey)
    {
        $currentTime = new DateTime();

        $apiUserType = $this->Admin_type_m->get_by(['machine_name' => 'API'], true);

        $currentUser = $this->Admin_m->get_by(['user_type' => $apiUserType->id, 'access_key' => $accessKey], true);

        $message = [];

        if (!empty($currentUser)) {
            $expireTime = new DateTime($currentUser->timeout);

            if ($currentTime > $expireTime) {
                $message = [
                    'status' => 'error',
                    'message' => 'Access token is expired. Please obtain a new token',
                    'data' => [],
                ];

            } else {
                $userData = [
                    'id' => $currentUser->id,
                    'name' => $currentUser->name,
                    'email' => $currentUser->email,
                ];

                $message = [
                    'status' => 'success',
                    'message' => 'Access token is valid',
                    'data' => $userData,
                ];
            }
        } else {
            $message = [
                'status' => 'error',
                'message' => 'Access token is invalid. Please obtain a new token',
                'data' => [],
            ];
        }

        return $message;
    }

    /**
     * This function is to create a new access token for a outside API user. This is also 
     * used to update the current expired token with a new token. [POST]
     * @return JSON
     */
    public function register()
    {
        $header = $this->input->request_headers();

        if (!empty($header["Application-Key"]) && $header['Application-Key'] == self::API_KEY) {
            if ($this->input->method() == 'post') {
                $message = [];

                $name = $this->input->post('name');
                $email = $this->input->post('email');

                if ($name == null || $email == null) {
                    $this->saveApiLog([], 'N', '403', 'Please provide name and email both');
                    $message = [
                        'status' => false,
                        'message' => 'Please provide name and email both'
                    ];
                } else {
                    $apiUserType = $this->Admin_type_m->get_by(['machine_name' => 'API'], true);

                    $randomNumber = time() * mt_rand (101,999) * mt_rand (101,999);
                    $accessKey = $this->Admin_m->hash($randomNumber);

                    $startDate = time();
                    $timeout = date('Y-m-d H:i:s', strtotime('+1 day', $startDate));

                    $saveArray = [
                        'name' => $name,
                        'email' => $email,
                        'access_key' => $accessKey,
                        'timeout' => $timeout,
                        'user_type' => $apiUserType->id,
                        'enable' => 1
                    ];

                    $existingAccessToken = !empty($header["Access-Key"]) ? $header["Access-Key"] : null;

                    $existingUserObj = $this->Admin_m->get_by(['email' => $email], true);

                    if (!empty($existingUserObj)) {
                        if ($existingUserObj->access_key != null && $existingUserObj->access_key == $existingAccessToken) {
                            $result = $this->Admin_m->update_by(['access_key' => $accessKey, 'timeout' => $timeout], ['id' => $existingUserObj->id]);
                            $message = [
                                'status' => true,
                                'message' => $accessKey
                            ];
                        } else {
                            $message = [
                                'status' => false,
                                'message' => 'There is a already registered user for the email. Please send the existing access key to renew.'
                            ];
                        }
                    } else {
                        $result = $this->Admin_m->save($saveArray);
                        $message = [
                            'status' => true,
                            'message' => $accessKey
                        ];
                    }
                }

                $this->saveApiLog([], 'Y', '200');
                $this->output
                   ->set_content_type('application/json')
                   ->set_output(json_encode($message));
            } else {
                $this->saveApiLog([], 'N', '405', 'Method not allowed');
                $this->output
                   ->set_content_type('application/json')
                   ->set_output(json_encode(['status' => 'error', 'message' => 'Method not allowed', 'data' => []]));
            }
        } else {
            $this->saveApiLog([], 'N', '403', 'Authentication Key could not be found or invalid Authentication Key provided');
            $this->output
               ->set_content_type('application/json')
               ->set_output(json_encode(['status' => 'error', 'message' => 'Authentication Key could not be found or invalid Authentication Key provided', 'data' => []]));
        }
    }

    /**
     * This function is to save every API call in the database with the 
     * relevant details
     * @param  array  $params
     * @param  string $authorized
     * @param  string $code
     * @param  string $message
     * @return int
     */
    private function saveApiLog($params = [], $authorized = 'Y', $code = '200', $message = null)
    {
        $ipAddress = null;

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //if ip is from share internet
            $ipAddress = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //if ip is from proxy
            $ipAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            //if ip is from remote address
            $ipAddress = $_SERVER['REMOTE_ADDR'];
        }

        $saveArray = [
            'uri' => uri_string(),
            'method' => $this->uri->segment(2),
            'params' => !empty($params) ? json_encode($params) : null,
            'ip_address' => $ipAddress,
            'time' => time(),
            'authorized' => $authorized,
            'response_code' => $code,
            'message' => $message
        ];

        return $this->Api_log_m->save($saveArray);
    }
}
