<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Assignment extends Admin_Controller
{

    public function index()
    {
        $assignments     = $this->Assignment_m->get_by(['status' => 1]);

        $this->data['assignments'] = $assignments;
        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    public function add()
    {
        if ($this->input->method() == 'post') {
            $postArray = array('assignment_name', 'status');

            $postdata = $this->Assignment_m->array_from_post($postArray);

            $this->Assignment_m->save($postdata);
            $this->session->set_flashdata('success', 'Assignment successfully created');
            redirect('Assignment');
        }

        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    public function edit($id = null)
    {
        if ($id != null) {
            $assignmentObj = $this->Assignment_m->get($id);
            if (!empty($assignmentObj)) {
                if ($this->input->method() == 'post') {
                    $postArray = array('assignment_name', 'status');

                    $postdata = $this->Assignment_m->array_from_post($postArray);

                    $this->Assignment_m->update_by($postdata, ['id' => $id]);

                    $this->session->set_flashdata('success', 'Assignment successfully updated');
                    redirect('Assignment');
                }

                $this->data['assignment']      = $assignmentObj;
            } else {
                $this->data['error']['message']      = 'This assignment does not exists.';    
            }
        } else {
            $this->data['error']['message']      = 'Please provide an id for the assignment';
        }

        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    public function question($id = null)
    {
        if ($id != null) {
            $assignmentObj = $this->Assignment_m->get($id);
            if (!empty($assignmentObj)) {

                if ($this->input->method() == 'post') {
                    $postArray = array('question');

                    $postdata = $this->Assignment_question_m->array_from_post($postArray);

                    $saveArray = [
                        'assignment_id' => $id,
                        'question' => $postdata['question']
                    ];

                    $this->Assignment_question_m->save($saveArray);

                    $this->session->set_flashdata('success', 'Question successfully added');
                    redirect('Assignment');
                }

                $this->data['assignment']      = $assignmentObj;
            } else {
                $this->data['error']['message']      = 'This assignment does not exists.';    
            }
        } else {
            $this->data['error']['message']      = 'Please provide an id for the assignment';
        }

        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    public function view($assignmentId = null)
    {
        if ($assignmentId != null) {
            $assignmentObj = $this->Assignment_m->get($assignmentId);
            if (!empty($assignmentObj)) {
                $classAssignment = $this->Class_assignment_m->get_by(['assignment_id' => $assignmentId]);

                $classIdsList = [];

                foreach ($classAssignment as $class) {
                    if (!array_key_exists($class->class_id, $classIdsList)) {
                        $classIdsList[$class->class_id] = $class->class_id;
                    }
                }

                $studentClass = [];
                $studentIdList = [];
                $students = [];
                if (!empty($classIdsList)) {
                    $studentClass = $this->Student_class_m->get_in('class_id', $classIdsList);

                    $studentIdList = [];

                    foreach ($studentClass as $student) {
                        if (!array_key_exists($student->student_id, $studentIdList)) {
                            $studentIdList[$student->student_id] = $student->student_id;
                        }
                    }

                    $students = $this->Student_m->get_in('id', $studentIdList);
                }

                $this->data['students']      = $students;
                $this->data['assignmentId']      = $assignmentId;
                $this->data['assignmentObj']      = $assignmentObj;
            } else {
                $this->data['error']['message']      = 'This assignment does not exists.';    
            }
        } else {
            $this->data['error']['message']      = 'Please provide an id for the assignment';   
        }

        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    public function getResultsData()
    {
        if ($this->input->method() == 'post') {
            $assignment_id = $this->input->post('assignment_id');
            $student_id = $this->input->post('student_id');

            $studentObj = $this->Student_m->get($student_id);
            $resultDataObjs = $this->Results_data_m->get_by(['assignment_id' => $assignment_id, 'student_id' => $student_id]);
            $structureddata['results']  = $this->Assignment_m->structureAssignmentResultData($resultDataObjs);
            $structureddata['student_name']  = $studentObj->student_name;

            echo json_encode($structureddata);
        }
    }

    public function viewQuestions()
    {
        if ($this->input->method() == 'post') {
            $assignmentId = $this->input->post('assignment_id');
            $assignmentObj = $this->Assignment_m->get($assignmentId);

            $questions = $this->Assignment_question_m->get_by(['assignment_id' => $assignmentId, 'status' => 1]);

            $returnArray = [
                'assignment' => $assignmentObj,
                'questions' => $questions
            ];

            echo json_encode($returnArray);
        }
    }
}
