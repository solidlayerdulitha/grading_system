<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Teacher extends Admin_Controller
{
    /**
     * View overall grading for assignments and answers
     * submitted by student.
     */
    public function index()
    {
        
        $classes = $this->Class_m->get();
        $classAssignmentsList = $this->Class_assignment_m->get();
        $assignments = $this->Assignment_m->get();
        $studentList = $this->Student_class_m->get();

        $this->data['classes'] = $classes;
        $this->data['assignments'] = $assignments;
        $this->data['studentList'] = $studentList;
        $this->data['classAssignmentsList'] = $classAssignmentsList;
        $this->data['subview']      = $this->_view;
        $this->load->view($this->_template . '/_layout_main', $this->data);
    }

    /**
     * Analysis of questions with results [AJAX] 
     * @return JSON
     */
    public function getQuestionAnalysis()
    {
        if ($this->input->method() == 'post') {
            $answersList = [];
            $assignmentId = $this->input->post('assignment_id');

            $questions = $this->Assignment_question_m->get_by(['assignment_id' => $assignmentId]);
            $questionsDropDown = $this->Assignment_question_m->dropdown_list('question', 'Select Class', ['assignment_id' => $assignmentId], 'id', false);

            $questionsIdsList = [];

            foreach ($questions as $question) {
                if (!array_key_exists($question->id, $questionsIdsList)) {
                    $questionsIdsList[$question->id] = $question->id;
                }
            }
            if (!empty($questionsIdsList)) {
                $answersList = $this->Answer_data_m->get_in('question_id', $questionsIdsList);
            }
            
            $structuredData = $this->Teacher_m->structureAnswerData($answersList, $questionsDropDown);


            echo json_encode($structuredData);
        }
    }

    /**
     * Overall statistics for questions [AJAX]
     * @return JSON
     */
    public function getQuestionStats()
    {
        if ($this->input->method() == 'post') {
            $questionId = $this->input->post('question_id');
            $assignmentId = $this->input->post('assignment_id');

            $answerDataObj = $this->Answer_data_m->get_by(['question_id' => $questionId, 'assignment_id' => $assignmentId]);

            $resultObj = $this->Results_data_m->get();
            $resultObj = $this->Results_data_m->structureArrayWithGivenKey($resultObj);

            $studentDropdown = $this->Student_m->dropdown_list('student_name', '', [], 'id', false);

            $strcturedData = $this->Teacher_m->structureAnswerStats($answerDataObj, $studentDropdown, $resultObj);

            echo json_encode($strcturedData);
        }
    }
}
