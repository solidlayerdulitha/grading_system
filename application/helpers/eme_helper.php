<?php
function btn_delete($uri){
    return anchor($uri, '<i class="icon-remove"></i>', array(
        'onclick' => "return confirm('Are you sure you want to delete this User?');")
    );
}

function btn_edit($uri){
	$url = base_url().$uri;
	return anchor($url, '&nbsp Edit &nbsp;', array(
		'class' => "btn btn-primary"));
}


function btn_save($tabindex = null){
    return form_submit('submit','Save', 'class="btn btn-success" id="saveRecord" tabindex="'.$tabindex.'"'); 
}

function btn_update(){
	return form_submit('submit','Update', 'class="btn btn-success" id="updateRecord"'); 
}

function btn_cancel($uri, $tabindex = null){
	$url = base_url().$uri;
	return anchor($url, 'Cancel', array(
		'class' => "btn btn-default",
        'tabindex' => $tabindex));
}

function btn_back($uri){
	$url = base_url().$uri;
	return anchor($url, 'Back', array(
		'class' => "btn btn-default"));
}