<?php

/**
 * Aws Helper
 *
 * Long description for class (if any)...
 * @author     Dulitha Rajapaksha <dulithamahishka94@gmail.com>
 * @copyright  2020 Dulitha Rajapaksha
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 5.0.0
 */

require '../vendor/autoload.php';

use Aws\S3\S3Client;
use Aws\Result;
use Aws\S3\Exception\S3Exception;
use Aws\CommandPool;

/**
 * returns AWS CDN URL (can be used to retrieve files in S3 bucket)
 *
 * @return string AWS CDN URL
 */
function aws_cdn_url()
{
	return "";
}

/**
 * Connect to AWS (get S3 client)
 *
 * @return array [s3client object, bucketName string]
 */
function getS3Client()
{

	//AWS credintials
	$bucketName = '';
	$key        = '';
	$secret     = '';
	$region     = '';
	$version    = 'latest';

	try {

		//connect to AWS
		$s3Client = S3Client::factory(
			[
				'credentials' => [
					'key'    => $key,
					'secret' => $secret
				],
				'version' => $version,
				'region'  => $region
			]
		);

		return [
			's3Client'   => $s3Client,
			'bucketName' => $bucketName
		];

	} catch (S3Exception $e) {
		throw new Exception($e->getMessage());
	}
	catch (Exception $e) {
		throw new Exception($e->getMessage());
	}

}

/**
 * Upload single file to S3 bucket
 *
 * @param file $file ($_FILES['tmp_name'])
 * @param string $path
 *
 * If the file is uploading to the root: $path = 'file_name.jpg'
 * If the file is uploading to a directory: $path = 'directory_name/file_name.jpg'
 *
 * @return string returns containing the file path when the file upload is completed
 *         
 */
function uploadToS3($file, $path)
{
	try {

		$s3ClientData  = getS3Client();
		$s3Client      = $s3ClientData['s3Client'];
		$bucketName    = $s3ClientData['bucketName'];

	    $result = $s3Client->putObject([
	        'Bucket'     => $bucketName,
	        'Key'        => $path,
	        'SourceFile' => $file,
	        'ACL'        => 'public-read'
    	]);

	    // get result status
	    $status = $result["@metadata"]["statusCode"];

	    if ($status == '200') {
				$objectUrl = $result['ObjectURL'];
				$urlPath   = parse_url($objectUrl, PHP_URL_PATH);

				return $urlPath;

	    } else {
	    	return '/dummy.png';
	    }


	} catch (S3Exception $e) {
		return '/dummy.png';
	}
	catch (Exception $e) {
		return '/dummy.png';
	}
}

/**
 * Upload multiple files to S3 bucket
 *
 * @param array $file ['directory_name/file_name.jpg' => $_FILES['tmp_name']]
 * @return string returns upload succeeded path names 
 *         
 */
function uploadToS3Multiple($files)
{
	try {

		$s3ClientData  = getS3Client();
		$s3Client      = $s3ClientData['s3Client'];
		$bucketName    = $s3ClientData['bucketName'];
		$commands      = [];

		//create commands array of command objects
		foreach ($files as $path => $file) {
			$commands[] = $s3Client->getCommand('PutObject', [
			    'Bucket'     => $bucketName,
			    'Key'        => $path,
    	        'SourceFile' => $file,
	        	'ACL'        => 'public-read'
			]);
		}

		// Execute an array of command objects to do them in parallel
		$results     = CommandPool::batch($s3Client, $commands);
		$returnPaths = []; 

		// Loop over the command results
		foreach ($results as $result) {

			$status = $result["@metadata"]["statusCode"];

		    if ($status == '200') {
				$objectUrl     = $result['ObjectURL'];
				$urlPath       = parse_url($objectUrl, PHP_URL_PATH);
		    	$returnPaths[] = $urlPath;

		    } else {
		    	$returnPaths[] = '/dummy.png';
		    }
		}

		return $returnPaths;

	} catch (S3Exception $e) {
		return [];
	}
	catch (Exception $e) {
		return [];	
	}
}