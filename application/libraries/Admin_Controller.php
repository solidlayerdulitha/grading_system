<?php
/**
 * Short description for class
 *
 * Long description for class (if any)...
 * @author     Dulitha Rajapaksha <dulithamahishka94@gmail.com>
 * @copyright  Dulitha Rajapaksha
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 1.0.0
 */

class Admin_Controller extends MY_Controller
{
    function __construct(){
        parent::__construct();
        $this->data['meta_title'] = 'OpenPHP';
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Admin_m');
        $this->load->model('Admin_type_m');
        $this->load->model('Users_m');
        $this->load->model('Role_m');
        $this->load->model('Assignment_m');
        $this->load->model('Class_m');
        $this->load->model('Student_m');
        $this->load->model('Student_class_m');
        $this->load->model('Class_assignment_m');
        $this->load->model('Assignment_question_m');
        $this->load->model('Results_data_m');
        $this->load->model('Answer_data_m');
        $this->load->model('Teacher_m');
        $this->load->model('Api_log_m');

        //login check
        $exception_urls = array(
            'User/login',
            'User/logout',
            'Api/question',
            'Api/answers',
            'Assignment/getResultsData',
            'Teacher/getQuestionAnalysis',
            'Teacher/getQuestionStats',
            'Api/abc',
            'Api/assignment',
            'Api/student',
            'Api/register',
            'Assignment/viewQuestions',
        );

        if(in_array(uri_string(), $exception_urls) == FALSE){ // if we are not in exception URLs do the check
            if($this->Admin_m->logged_in() == FALSE){
                redirect('User/login');
            }
        }

        //check whether the logged in user is a student. If yes redirected to 404
        $studentTypeObj = $this->Admin_type_m->get_by(['machine_name' => 'STUDENT'], true);

        if ($this->session->userdata('user_type') == $studentTypeObj->id) {
            redirect('NotFound');
        }

    }
}
