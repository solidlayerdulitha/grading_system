<section class="content-header">
	<h1>
		Classes List
	</h1>
	Assignments according to added classes are listed here.
</section>
<!-- Main content -->
<section class="content teacher-index">
	<div class="row">
		<div class="col-xs-12">
			<?php if ($this->session->flashdata('success')) { ?>
				<div class="alert alert-success alert-dismissible">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>

			<?php if ($this->session->flashdata('error')) { ?>
				<div class="alert alert-danger alert-dismissible">
					<?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php } ?>

			<?php foreach ($classes as $class): ?>
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><?php echo $class->class_name ?></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>
										Assignment Name
									</th>
									<th>
										Action
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($classAssignmentsList as $assignment): ?>
									<?php if ($assignment->class_id == $class->id): ?>
										<tr>
											<td><?php echo $assignment->assignment_name ?></td>
											
											<td>
												<a href="<?php echo base_url() . 'Assignment/view/' . $assignment->assignment_id ?>" >View Assignment</a> | 
												<a href="javascript:void(0)" class="overall_grading" data-assignment="<?php echo $assignment->assignment_id ?>">Overall Grading</a> 
											</td>
										</tr>
									<?php endif ?>
								<?php endforeach ?>
							</tbody>
						</table>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			<?php endforeach ?>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>

<div class="modal fade" id="resultsModal" tabindex="-1" role="dialog" aria-labelledby="resultsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="resultsModalLabel">Overall answers for the questions</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<table class="table table-bordered table-striped">
      		<thead>
				<tr>
					<th>
						Question
					</th>
					<th>
						Right
					</th>
					<th>
						Wrong
					</th>
					<th>
						Partial
					</th>
				</tr>
			</thead>
			<tbody id="result-modal-body">
				
			</tbody>
      	</table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="answerModal" tabindex="-1" role="dialog" aria-labelledby="answerModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="answerModalLabel">Answer statistics</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<table class="table table-bordered table-striped">
      		<thead>
				<tr>
					<th>
						Student Name
					</th>
					<th>
						Answer
					</th>
					<th>
						Result
					</th>
					<th>
						Attempt No
					</th>
				</tr>
			</thead>
			<tbody id="answer-modal-body">
				
			</tbody>
      	</table>
      </div>
    </div>
  </div>
</div>


