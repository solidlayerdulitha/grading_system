
<div class="login-box">
  <div class="login-logo">
    <a href="<?php print base_url(); ?>"><b>Grading System<br/></b> Portal</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Welcome to the Grading System</p>

    <form action="" method="post" id="login-form">
    <?php echo validation_errors('<div class="alert" style="margin-bottom: 10px; color: red;">','</div>'); ?>
    <?php $error = $this->session->flashdata('error');
    if(!empty($error)){ ?>
        <div class="alert" style="margin-bottom: 10px; color: red;"><?php echo $error; ?></div>
    <?php }?>
      <div class="alert email-field-check" style="margin-bottom: 10px; color: red; display: none;">Email cannot be empty</div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control login-email" placeholder="Email" name="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="alert pw-field-check" style="margin-bottom: 10px; color: red; display: none;">Password cannot be empty</div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control login-pw" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <hr>
      <!-- <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a> -->
    </div>
    <!-- /.social-auth-links -->

    <!-- <a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>