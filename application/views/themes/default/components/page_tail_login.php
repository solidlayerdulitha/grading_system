<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url().'/themes/'.$this->config->item('template_files'); ?>/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url().'/themes/'.$this->config->item('template_files'); ?>/dist/js/custom.js?v=<?=$this->config->item("js_curr_ver");?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url().'/themes/'.$this->config->item('template_files'); ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url().'/themes/'.$this->config->item('template_files'); ?>/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
