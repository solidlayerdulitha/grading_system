<!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2020 Dulitha Rajapaksha.</strong> 
  </footer>

  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script src="https://SortableJS.github.io/Sortable/Sortable.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="<?php echo base_url().'/themes/'.$this->config->item('template_files'); ?>/dist/js/custom.js?v=<?=$this->config->item("js_curr_ver");?>"></script>

<script>
  $(function () {
    $('#example1').DataTable();
    $('#example3').DataTable();
    $('#data-table-1').DataTable();
    $('#data-table-2').DataTable();
    $('#data-table-3').DataTable();
    $('#data-table-4').DataTable();
    $('#data-table-5').DataTable();
    $('#data-table-6').DataTable();
    $('#data-table-7').DataTable();
    $('#client-user').DataTable();
    
    new Sortable(sortable1ex, {
      animation: 150,
      ghostClass: 'blue-background-class'
    });

    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  })

  
</script>
</body>
</html>
