<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="<?php echo base_url() . '/themes/' . $this->config->item('template_files'); ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p><?php echo $this->session->userdata('name'); ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $permissionArr = $this->session->userdata('permission'); ?>
        <?php $urlSegment = $this->uri->segment(1); ?>

        <?php if (in_array('Assignment', $permissionArr)): ?>
            <li class="<?php echo ($urlSegment == 'Assignment') ? 'active' : '' ?>">
                <a href="<?php echo base_url(); ?>Assignment">
                    <i class="fa fa-check"></i>
                    <span>Assignments</span>
                </a>
            </li>
        <?php endif ?>
        <?php if (in_array('ClassType', $permissionArr)): ?>
            <li class="<?php echo ($urlSegment == 'ClassType') ? 'active' : '' ?>">
                <a href="<?php echo base_url(); ?>ClassType">
                    <i class="fa fa-check"></i>
                    <span>Class Details</span>
                </a>
            </li>
        <?php endif; ?>
        <?php if (in_array('Student', $permissionArr)): ?>
            <li class="<?php echo ($urlSegment == 'Student') ? 'active' : '' ?>">
                <a href="<?php echo base_url(); ?>Student">
                    <i class="fa fa-check"></i>
                    <span>Student List</span>
                </a>
            </li>
        <?php endif; ?>
        <?php if (in_array('Profile', $permissionArr)): ?>
            <li class="<?php echo ($urlSegment == 'Profile') ? 'active' : '' ?>">
                <a href="<?php echo base_url(); ?>Profile">
                    <i class="fa fa-check"></i>
                    <span>Student Profile</span>
                </a>
            </li>
        <?php endif; ?>
        <?php if (in_array('Teacher', $permissionArr)): ?>
            <li class="<?php echo ($urlSegment == 'Teacher') ? 'active' : '' ?>">
                <a href="<?php echo base_url(); ?>Teacher">
                    <i class="fa fa-check"></i>
                    <span>Overall Grading</span>
                </a>
            </li>
        <?php endif; ?>
    </ul>
</section>
