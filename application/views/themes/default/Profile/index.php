<section class="content-header">
	<h1>
		<?php echo $studentData->student_name ?>
	</h1>
	You can find all your assignments with the submitted answers and details.
</section>
<!-- Main content -->
<section class="content profile-index">
	<div class="row">
		<div class="col-xs-12">
			<?php if ($this->session->flashdata('success')) { ?>
				<div class="alert alert-success alert-dismissible">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>

			<?php if ($this->session->flashdata('error')) { ?>
				<div class="alert alert-danger alert-dismissible">
					<?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php } ?>

			<?php foreach ($assignments as $assignment): ?>
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><?php echo $assignment->assignment_name ?></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered table-striped" id="example1">
							<thead>
								<tr>
									<th>
										Overall Marks
									</th>
									<th>
										Time Spent for the assignment (HH:MM:SS)
									</th>
									<th>
										Attempt Number
									</th>
									<th>
										Action
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($examResults as $result): ?>
									<?php if ($result->assignment_id == $assignment->id): ?>
										<tr>
											<td><?php echo $result->result ?></td>
											<td>
												<?php
													$datetime1 = new DateTime($result->time_start);
													$datetime2 = new DateTime($result->time_end);
													$interval = $datetime1->diff($datetime2);
													echo $interval->format('%H:%i:%s');
												?>
											</td>
											<td><?php echo $result->attempt_number ?></td>
											<td><a href="javascript:void(0)" class="review-answers" data-result="<?php echo $result->id ?>" data-stu="<?php echo $result->student_id ?>">Review My Answers</a></td>
										</tr>
									<?php endif ?>
								<?php endforeach ?>
							</tbody>
						</table>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			<?php endforeach ?>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>

<div class="modal fade" id="resultsModal" tabindex="-1" role="dialog" aria-labelledby="resultsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="resultsModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<table class="table table-bordered table-striped">
      		<thead>
				<tr>
					<th>
						Question
					</th>
					<th>
						Answer
					</th>
					<th>
						Result
					</th>
				</tr>
			</thead>
			<tbody id="result-modal-body">
				
			</tbody>
      	</table>
      </div>
    </div>
  </div>
</div>


