<section class="content-header">
	<h1>
		Added assignments for <?php echo $classObj->class_name ?>
	</h1>
	Assignments that have been added to the class are listed here
	<ol class="breadcrumb">
		<li>
			<a class="addRecord btn btn-primary" href="<?php echo base_url($current_class . '/assign/' . $classObj->id); ?>">
				<span class="glyphicon glyphicon-plus">
				</span>
				Add Assignment
			</a>
		</li>
	</ol>
	<br>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<?php if ($this->session->flashdata('success')) { ?>
				<div class="alert alert-success alert-dismissible">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>

			<?php if ($this->session->flashdata('error')) { ?>
				<div class="alert alert-danger alert-dismissible">
					<?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php } ?>
			<div class="box">
				<div class="box-header">
					<!-- <h3 class="box-title">Data Table With Full Features</h3> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-bordered table-striped" id="example1">
						<thead>
							<tr>
								<th>
									Assignment Name
								</th>
								<th>
									Status
								</th>								
							</tr>
						</thead>
						<tbody>
							<?php foreach ($classAssignments as $class): ?>
								<tr>
									<td><?php echo $class->assignment_name ?></td>
									<td>
										<?php 
				                      if ($class->status == 1) {
				                        echo '<span class="label label-success">Active</span>';
				                      } else {
				                        echo '<span class="label label-danger">Inactive</span>';
				                      }
				                    ?>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>


