<?php if (!isset($error)): ?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Assign assignments to a class : <?php echo $classObj->class_name ?>
        <small></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box box-primary">
            <?php echo form_open();?>
            <div class="box-body">

              <?php echo validation_errors('<div class="alert alert-error" style="margin-bottom: 10px;"><button type="button" class="close" data-dismiss="alert">×</button>','</div>'); ?>

              <table class="table">
                <tr>
                      <?php $inc = 1; ?>
                      <?php foreach ($assignments as $assignment): ?>
                        <?php if (!array_key_exists($assignment->id, $classAssignments)): ?>
                          <?php if ($inc == 1): ?>
                            <tr>
                          <?php endif ?>

                          <td class="tablefield"><?php echo $assignment->assignment_name ?> <span class="required">*</span></td>
                          <td class="tabledata"><?php echo form_checkbox('assignment_'.$assignment->id, 1, set_value('assignment_"'.$assignment->id.'"',''), 'class="minimal-red"'); ?>
                          </td>

                          <?php if ($inc == 2): ?>
                            </tr>
                          <?php $inc = 1; ?>
                          <?php else: ?>
                            <?php $inc++; ?>
                          <?php endif ?>
                        <?php endif ?>
                      <?php endforeach ?>
                </tr>
              </table>
            </div>
            <div class="box-footer">
              <?php echo btn_cancel($current_class); ?>
              <?php echo btn_save(); ?>
            </div>
            <?php echo form_close();?>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <?php else: ?>
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <?php echo $error['message']; ?>
            </div>
          </div>
      </div>
    </section>
  <?php endif ?>
