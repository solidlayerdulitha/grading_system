<section class="content-header">
	<h1>
		Class List
	</h1>
	All the class in the system are listed here. Currently class are added straight to database.
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<?php if ($this->session->flashdata('success')) { ?>
				<div class="alert alert-success alert-dismissible">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>

			<?php if ($this->session->flashdata('error')) { ?>
				<div class="alert alert-danger alert-dismissible">
					<?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php } ?>
			<div class="box">
				<div class="box-header">
					<!-- <h3 class="box-title">Data Table With Full Features</h3> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-bordered table-striped" id="example1">
						<thead>
							<tr>
								<th>
									Class Name
								</th>
								<th>
									Status
								</th>
								<th>
									Action
								</th>								
							</tr>
						</thead>
						<tbody>
							<?php foreach ($classes as $class): ?>
								<tr>
									<td><?php echo $class->class_name ?></td>
									<td>
										<?php 
				                      if ($class->status == 1) {
				                        echo '<span class="label label-success">Active</span>';
				                      } else {
				                        echo '<span class="label label-danger">Inactive</span>';
				                      }
				                    ?>
									</td>
									<td>
										<a href="<?php echo base_url() . $current_class. '/view/'?><?php echo $class->id ?>"><i class="fa fa-edit"></i> View Assignments</a>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>


