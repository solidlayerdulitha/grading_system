<?php if (!isset($error)): ?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Assignment
        <small></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box box-primary">
            <?php echo form_open();?>
            <div class="box-body">

              <?php echo validation_errors('<div class="alert alert-error" style="margin-bottom: 10px;"><button type="button" class="close" data-dismiss="alert">×</button>','</div>'); ?>

              <table class="table">
                <tr>
                  <td class="tablefield">Assignment Name <span class="required">*</span></td>
                  <td class="tabledata"><?php echo form_input('assignment_name',set_value('assignment_name',''),'id="assignment_name" class="form-control" tabindex="1"'); ?>
                  </td>
                  <td class="tablefield">Status <span class="required">*</span></td>
                  <td class="tabledata">
                    <?php
                      $statusOptions = array(
                          '1'    => 'Active',
                          '0'    => 'Inactive',
                      );

                     echo form_dropdown('status', $statusOptions, '','tabindex="9" class="form-control"'); ?>
                  </td>
                </tr>
              </table>
            </div>
            <div class="box-footer">
              <?php echo btn_cancel($current_class); ?>
              <?php echo btn_save(); ?>
            </div>
            <?php echo form_close();?>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <?php else: ?>
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <?php echo $error['message']; ?>
            </div>
          </div>
      </div>
    </section>
  <?php endif ?>
