<section class="content-header">
	<h1>
		Students for <?php echo $assignmentObj->assignment_name ?>
	</h1>

	These students enrolled in the class and have access to assignments
</section>
<!-- Main content -->
<section class="content assignment-student-view">
	<div class="row">
		<div class="col-xs-12">
			<?php if ($this->session->flashdata('success')) { ?>
				<div class="alert alert-success alert-dismissible">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>

			<?php if ($this->session->flashdata('error')) { ?>
				<div class="alert alert-danger alert-dismissible">
					<?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php } ?>

			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Students</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>
									Student Name
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($students as $student): ?>
								<tr>
									<td><?php echo $student->student_name ?></td>
									
									<td><a href="javascript:void(0)" class="student_view_assignments" data-stu="<?php echo $student->id ?>" data-assignment="<?php echo $assignmentId ?>">View Details</a></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
			<div class="box-footer">
              <?php echo btn_cancel('Teacher'); ?>
            </div>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>

<div class="modal fade" id="resultsModal" tabindex="-1" role="dialog" aria-labelledby="resultsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="resultsModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<table class="table table-bordered table-striped">
      		<thead>
				<tr>
					<th>
						Attempts
					</th>
					<th>
						Average Time Spent
					</th>
					<th>
						Average Result
					</th>
				</tr>
			</thead>
			<tbody id="result-modal-body">
				
			</tbody>
      	</table>
      </div>
    </div>
  </div>
</div>


