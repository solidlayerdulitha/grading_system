<section class="content-header">
	<h1>
		Assignment List
	</h1>
	All assignments in the system are listed here
	<ol class="breadcrumb">
		<li>
			<a class="addRecord btn btn-primary" href="<?php echo base_url($current_class . '/add'); ?>">
				<span class="glyphicon glyphicon-plus">
				</span>
				Add Assignment
			</a>
		</li>
	</ol>
	<br>
</section>
<!-- Main content -->
<section class="content assignment-index">
	<div class="row">
		<div class="col-xs-12">
			<?php if ($this->session->flashdata('success')) { ?>
				<div class="alert alert-success alert-dismissible">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>

			<?php if ($this->session->flashdata('error')) { ?>
				<div class="alert alert-danger alert-dismissible">
					<?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php } ?>
			<div class="box">
				<div class="box-header">
					<!-- <h3 class="box-title">Data Table With Full Features</h3> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-bordered table-striped" id="example1">
						<thead>
							<tr>
								<th>
									Assignment Name
								</th>
								<th>
									Status
								</th>
								<th>
									Action
								</th>								
							</tr>
						</thead>
						<tbody>
							<?php foreach ($assignments as $assignment): ?>
								<tr>
									<td><?php echo $assignment->assignment_name ?></td>
									<td>
										<?php 
				                      if ($assignment->status == 1) {
				                        echo '<span class="label label-success">Active</span>';
				                      } else {
				                        echo '<span class="label label-danger">Inactive</span>';
				                      }
				                    ?>
									</td>
									<td>
										<a href="<?php echo base_url() . $current_class. '/edit/'?><?php echo $assignment->id ?>"><i class="fa fa-edit"></i> Edit</a> | 
										<a href="<?php echo base_url() . $current_class. '/question/'?><?php echo $assignment->id ?>"><i class="fa fa-edit"></i> Add question</a> |
										<a href="javascript:void(0)" data-assignment="<?php echo $assignment->id ?>" class="assignment_view_questions"><i class="fa fa-edit"></i> View questions</a>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>

<div class="modal fade" id="resultsModal" tabindex="-1" role="dialog" aria-labelledby="resultsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="resultsModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<table class="table table-bordered table-striped">
      		<thead>
				<tr>
					<th>
						Question
					</th>
				</tr>
			</thead>
			<tbody id="result-modal-body">
			</tbody>
      	</table>
      </div>
    </div>
  </div>
</div>


