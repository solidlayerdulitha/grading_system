<?php $this->load->view('themes/' . $this->config->item('template') . '/components/page_head'); ?>
<script type="text/javascript">
	var base_url = '<?php echo base_url(); ?>';
</script>
            <!-- MAIN CONTENT-->
  			<!-- Content Wrapper. Contains page content -->
  			<div class="content-wrapper">

  				<?php $this->load->view($subview);?>


		    <!-- /.content -->
		    </div>


  <?php $this->load->view('themes/' . $this->config->item('template') . '/components/page_tail'); ?>