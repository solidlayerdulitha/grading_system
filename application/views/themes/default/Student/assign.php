<?php if (!isset($error)): ?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Assign an Assignment to a Student
        <small></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box box-primary">
            <?php echo form_open();?>
            <div class="box-body">

              <?php echo validation_errors('<div class="alert alert-error" style="margin-bottom: 10px;"><button type="button" class="close" data-dismiss="alert">×</button>','</div>'); ?>

              <table class="table">
                <tr>
                  <td class="tablefield">Assignment Name <span class="required">*</span></td>
                  <td class="tabledata"><?php echo form_input('assignment_name',set_value('assignment_name',''),'id="assignment_name" class="form-control" tabindex="1"'); ?>
                  </td>
                </tr>
                <tr>
                    <!-- <td class="tablefield">Select Students <span class="required">*</span></td> -->
                      <?php $inc = 1; ?>
                      <?php foreach ($students as $student): ?>
                        <?php if ($inc == 1): ?>
                          <tr>
                        <?php endif ?>

                        <td class="tablefield"><?php echo $student->name ?> <span class="required">*</span></td>
                        <td class="tabledata"><?php echo form_checkbox('student_'.$student->id, 1, set_value('student_"'.$student->id.'"',''), 'class="minimal-red"'); ?>
                        </td>

                        <?php if ($inc == 2): ?>
                          </tr>
                        <?php $inc = 1; ?>
                        <?php else: ?>
                          <?php $inc++; ?>
                        <?php endif ?>
                      <?php endforeach ?>
                </tr>
              </table>
            </div>
            <div class="box-footer">
              <?php echo btn_cancel($current_class); ?>
              <?php echo btn_save(); ?>
            </div>
            <?php echo form_close();?>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <?php else: ?>
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <?php echo $error['message']; ?>
            </div>
          </div>
      </div>
    </section>
  <?php endif ?>
