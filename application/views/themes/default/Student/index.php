<section class="content-header">
	<h1>
		Students List
	</h1>
	All students in the system are listed here
	<ol class="breadcrumb">
		<li>
			<a class="addRecord btn btn-primary" href="<?php echo base_url($current_class . '/add'); ?>">
				<span class="glyphicon glyphicon-plus">
				</span>
				Add Student
			</a>
		</li>
	</ol>
	<br>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<?php if ($this->session->flashdata('success')) { ?>
				<div class="alert alert-success alert-dismissible">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>

			<?php if ($this->session->flashdata('error')) { ?>
				<div class="alert alert-danger alert-dismissible">
					<?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php } ?>
			<div class="box">
				<div class="box-header">
					<!-- <h3 class="box-title">Data Table With Full Features</h3> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-bordered table-striped" id="example1">
						<thead>
							<tr>
								<th>
									Student Name
								</th>
								<th>
									Status
								</th>								
							</tr>
						</thead>
						<tbody>
							<?php foreach ($students as $student): ?>
								<tr>
									<td><?php echo $student->student_name ?></td>
									<td>
										<?php 
				                      if ($student->status == 1) {
				                        echo '<span class="label label-success">Active</span>';
				                      } else {
				                        echo '<span class="label label-danger">Inactive</span>';
				                      }
				                    ?>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>


