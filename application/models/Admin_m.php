<?php
/**
 * Admin_m Model
 *
 * Long description for class (if any)...
 * @author     Dulitha Rajapaksha <dulithamahishka94@gmail.com>
 * @copyright  2020 Dulitha Rajapaksha
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 5.0.0
 */


class Admin_m extends MY_Model
{
    protected $_table_name = 'admin';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = TRUE;
    public    $rules = array(
        'email' => array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|valid_email|max_length[50]'
        ),
        'password' => array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|max_length[50]'
        )
    );

    function __construct(){
        parent::__construct();
    }

    public function get_new(){
        $admin = new stdClass();
        $admin->name       = '';
        $admin->email      = '';
        $admin->password   = '';
        $admin->confirm_password  = '';
        $admin->user_type  = '';
        $admin->enable     = '';
        $admin->last_login = '';
        return $admin;
    }

    public function login(){
        //echo 'login model';
        $apiUserType = $this->Admin_type_m->get_by(['machine_name' => 'API'], true);

        $user = $this->get_by(array(
            'email' => $this->input->post('email'),
            'password' => $this->hash($this->input->post('password')),
            'user_type !=' => $apiUserType->id,
        ), TRUE); // true, because we want's single use object NOT array of objects

        if(count((array)$user)){
            // log in user
            $data = array(
                'name' => $user->name,
                'email' => $user->email,
                'id' => $user->id,
                'admin_loggedin' => TRUE,
                'user_type'=>$user->user_type,
            );
            $this->session->set_userdata($data);
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function logout(){
        $this->session->sess_destroy();
    }

    public function logged_in(){
        return (bool) $this->session->userdata('admin_loggedin');
    }

    public function hash($string){
        return hash('sha512', $string.config_item('encryption_key'));
    }
}