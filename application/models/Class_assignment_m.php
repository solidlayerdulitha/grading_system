<?php
/**
 * Class_assignment_m Model
 *
 * Long description for class (if any)...
 * @author     Dulitha Rajapaksha <dulithamahishka94@gmail.com>
 * @copyright  2020 Dulitha Rajapaksha
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 5.0.0
 */

class Class_assignment_m extends MY_Model
{
    protected $_table_name     = 'class_assignment';
    protected $_primary_key    = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by       = 'id';
    protected $_timestamps     = true;

    public function __construct()
    {
        parent::__construct();
    }

    public $rules_create = array(
        'assignment_name' => array(
            'field' => 'assignment_name',
            'label' => 'Assignment Name',
            'rules' => 'trim|required',
        ),
        'status'       => array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim|required|numeric|max_length[30]',
        ),
    );

    public $rules_edit = array(
        'assignment_name' => array(
            'field' => 'assignment_name',
            'label' => 'Assignment Name',
            'rules' => 'trim|required',
        ),
        'status'       => array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim|required|numeric|max_length[30]',
        ),
    );
}
