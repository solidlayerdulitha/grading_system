<?php
/**
 * Assignment_m Model
 *
 * Long description for class (if any)...
 * @author     Dulitha Rajapaksha <dulithamahishka94@gmail.com>
 * @copyright  2020 Dulitha Rajapaksha
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 5.0.0
 */

class Assignment_m extends MY_Model
{
    protected $_table_name     = 'assignment';
    protected $_primary_key    = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by       = 'id';
    protected $_timestamps     = true;

    public function __construct()
    {
        parent::__construct();
    }

    public $rules_create = array(
        'assignment_name' => array(
            'field' => 'assignment_name',
            'label' => 'Assignment Name',
            'rules' => 'trim|required',
        ),
        'status'       => array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim|required|numeric|max_length[30]',
        ),
    );

    public $rules_edit = array(
        'assignment_name' => array(
            'field' => 'assignment_name',
            'label' => 'Assignment Name',
            'rules' => 'trim|required',
        ),
        'status'       => array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim|required|numeric|max_length[30]',
        ),
    );

    public function structureAssignmentResultData($resultDataObjs)
    {
        $count = count($resultDataObjs);

        $totalSeconds = 0;
        $totalResult = 0;

        foreach ($resultDataObjs as $result) {
            $timeDiff = $this->getTimeDiff($result->time_start, $result->time_end);
            $diffInSeconds = $this->getSeconds($timeDiff);
            $totalSeconds += $diffInSeconds * 1;

            $totalResult += $result->result;
        }

        $averageTime = 'Not Submitted';
        $averageResult = 'Not Submitted';

        if ($count > 0) {
            $averageTime = $this->convertSecondsToHhMmSs($totalSeconds/$count);
            $averageResult = $totalResult/$count;
        } else {
            $count = 'No Attempts Recorded';
        }

        return [
            'no_of_attempts' => $count,
            'avg_time' => $averageTime,
            'avg_result' => $averageResult,
        ];
    }

    private function getSeconds($time)
    {
        sscanf($time, "%d:%d:%d", $hours, $minutes, $seconds);

        $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;

        return $time_seconds;
    }

    private function getTimeDiff($timeStart, $timeEnd)
    {
        $datetime1 = new DateTime($timeStart);
        $datetime2 = new DateTime($timeEnd);
        $interval = $datetime1->diff($datetime2);

        return $interval->format('%H:%i:%s');
    }

    private function convertSecondsToHhMmSs($seconds) {
      $t = round($seconds);
      return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
    }

}
