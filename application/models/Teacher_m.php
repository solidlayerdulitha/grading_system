<?php
/**
 * Teacher_m Model
 *
 * Long description for class (if any)...
 * @author     Dulitha Rajapaksha <dulithamahishka94@gmail.com>
 * @copyright  2020 Dulitha Rajapaksha
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 5.0.0
 */

class Teacher_m extends MY_Model
{
    protected $_table_name     = '';
    protected $_primary_key    = '';
    protected $_primary_filter = '';
    protected $_order_by       = '';
    protected $_timestamps     = true;

    public function __construct()
    {
        parent::__construct();
    }

    public function structureAnswerData($answersList, $questions)
    {
        $returnArr = [];

        foreach ($answersList as $answers) {
            if (array_key_exists($answers->question_id, $questions)) {
                if (!empty($returnArr[$answers->question_id])) {
                    if (array_key_exists($answers->result, $returnArr[$answers->question_id])) {
                        $returnArr[$answers->question_id][$answers->result] += 1;
                    }
                } else {
                    $returnArr[$answers->question_id] = [
                        'RIGHT' => ($answers->result == 'RIGHT') ? 1 : 0,
                        'WRONG' => ($answers->result == 'WRONG') ? 1 : 0,
                        'PARTIAL' => ($answers->result == 'PARTIAL') ? 1 : 0
                    ];
                    // $returnArr[$answers->question_id][$answers->result] = 1;
                }

                $returnArr[$answers->question_id]['question'] = $questions[$answers->question_id];
            }
        }

        return $returnArr;
    }

    public function structureAnswerStats($answerDataObj, $studentDropdown, $resultObj)
    {
        $returnArr = [];

        $colorArr = [
            'RIGHT'   => 'green',
            'WRONG'   => 'red',
            'PARTIAL' => 'orange',
        ];

        foreach ($answerDataObj as $answer) {
            if (array_key_exists($answer->student_id, $studentDropdown)) {
                $tmpArr = [];
                $tmpArr[$answer->student_id] = [
                    'student_name' => $studentDropdown[$answer->student_id],
                    'answer' => $answer->answer,
                    'result' => $answer->result,
                    'color'  => array_key_exists($answer->result, $colorArr) ? $colorArr[$answer->result] : ''
                ];

                if (array_key_exists($answer->result_data_id, $resultObj)) {
                    $tmpArr[$answer->student_id]['attempt'] = $resultObj[$answer->result_data_id]->attempt_number;
                }

                $returnArr[] = $tmpArr;
            }
        }

        return $returnArr;
    }
}
