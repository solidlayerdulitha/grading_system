<?php
/**
 * Profile_m Model
 *
 * Long description for class (if any)...
 * @author     Dulitha Rajapaksha <dulithamahishka94@gmail.com>
 * @copyright  2020 Dulitha Rajapaksha
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 5.0.0
 */

class Profile_m extends MY_Model
{
    protected $_table_name     = '';
    protected $_primary_key    = '';
    protected $_primary_filter = '';
    protected $_order_by       = '';
    protected $_timestamps     = true;

    public function __construct()
    {
        parent::__construct();
    }
    
    public function structureQuestionsAnswers($examAnswers, $examQuestions, $assignmentResult)
    {
    	$returnArr = [];

    	$colorArr = [
    		'RIGHT'   => 'green',
    		'WRONG'   => 'red',
    		'PARTIAL' => 'orange',
    	];

    	foreach ($examAnswers as $answer) {
    		$tmpArr['id'] = $answer->id;
    		$tmpArr['question'] = array_key_exists($answer->question_id, $examQuestions) ? $examQuestions[$answer->question_id] : '';
    		$tmpArr['answer'] = $answer->answer;
    		$tmpArr['results'] = $answer->result;
    		$tmpArr['color'] = array_key_exists($answer->result, $colorArr) ? $colorArr[$answer->result] : '';

    		$returnArr['answers'][] = $tmpArr;
    	}

    	$returnArr['assingment']['marks'] = $assignmentResult->result;

    	return $returnArr;
    }
    
}
