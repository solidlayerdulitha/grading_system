<?php
/**
 * Results_data_m Model
 *
 * Long description for class (if any)...
 * @author     Dulitha Rajapaksha <dulithamahishka94@gmail.com>
 * @copyright  2020 Dulitha Rajapaksha
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 5.0.0
 */

class Results_data_m extends MY_Model
{
    protected $_table_name     = 'results_data';
    protected $_primary_key    = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by       = 'id';
    protected $_timestamps     = true;

    public function __construct()
    {
        parent::__construct();
    }
}
