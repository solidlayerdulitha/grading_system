<?php

/**
 * -----------------------------IMPORTANT-------------------------------
 * Programmer should NOT change or add any code without having a better
 * understanding how MY_MODEL and Its methods been used
 * ---------------------------------------------------------------------
 *
 * My_Model will be used for all the CRUD operations in the system.
 *
 * All the other models should be extend form My_Model
 * Most of the common operations been written in the My_Model so that
 * programmer can easily call methods in My_Model Class for all most
 * all Database Communication and minimize the coding in their projects.
 *
 * @author     Dulitha Rajapaksha <dulithamahishka94@gmail.com>
 * @copyright  Dulitha Rajapaksha
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 1.0.0
 */
class MY_Model extends CI_Model {

    protected $_table_name = '';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = '';
    public $rules = array();
    protected $_timestamps = FALSE;

    function __construct() {
        parent::__construct();
    }


    /**
     * Purpose of the function is to reduce the code and minimize the error which counld occer
     * during the getting the form data using POST method.
     * in complex form when dealing with large amount of input programmer just need to send an
     * array of the form field list and then this function return all form data inputs as data array,
     * array key as the form input name and value as the form input value
     * example return:
     *        array(
     *         'first_name' => 'Dulitha',
     *         'last_name' => 'Rajapaksha'
     *        );
     *
     * @param  Array      $fields    list of Form feilds name.
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return array result set
     */
    public function array_from_post($fields) {
        $data = array();
        foreach ($fields as $field) {
            $data[$field] = $this->input->post($field);
        }
        return $data;
    }

    /**
     * Purpose of the function is to return a result set to used in the dropdown list.
     * array key will be the table ID , value will be the $field
     * example return:
     *          array(
     *              '1' => 'Sri Lanka',
     *              '2' => 'United Kingdon'
     *          );
     *
     * @param  Text         $field    feild name of the databse which reqired to be displayed in the value section in dropdown list.
     * @param  STDObject    $query    if additional query required it should be provided as STDObject example Array('country_name)'=>'Sri Lanka')
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return set of array which suite to use in dropdown boxs' dataset.
     */
    public function dropdown_list($field, $emptySelect = 'Please Select', $query = null, $id = 'id', $enableDropdownSelectTitle = true) {
        if ($query == null) {
            $records = $this->get();
        } else {
            $records = $this->get_by($query);
        }
        $fields_set = [];
        // setting the dropdown list array
        if ($enableDropdownSelectTitle) {
            $fields_set = array('' => $emptySelect);
        }

        // $fields_set = array(''=>''); // default/empty value
        // adding each result row to dropdown lisst array
        if (count($records)) {
            foreach ($records as $record) {
                $fields_set[$record->$id] = $record->$field;
            }
        }
        // reurn dropdown result set array
        return $fields_set;
    }

    /**
     * Purpose of the function is to return a specific record if '$id' is set which is the table primary key,
     * or return all records in specific table.
     * this function also used by the 'get_by' methods which will return single result as a row or
     * lines of result as a result set.
     *
     * @param  Integer  $id             ID of the table row which is looking for
     * @param  Boolean  $single         construct to help get_by method, If 'TRUE' meaning return single row, ELSE return result set array
     * @param  Text     $AsOrder        record set order, default set to 'Assending'
     * @param  Integer  $limit          Number of records limit, default set to NULL
     * @param  Integer  $pageNumber     page number *** this uses only in the pagination function is executed.***
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return row / all rows in the specific table
     */
    public function get($id = NULL, $single = NULL, $AsOrder = "asc", $limit = NULL, $pageNumber = 0) {
        try {
            if ($id != NULL) {
                $filter = $this->_primary_filter;
                $id = $filter($id);  // Added Security

                if (!$this->db->where($this->_primary_key, $id)) {
                    throw new Exception('error');
                }
                $method = 'row'; // single record
            } elseif ($single == TRUE) {
                $method = 'row'; // single record
            } else {
                $method = 'result'; // all record
            }

            if ($this->_order_by) {
                if (!$this->db->order_by($this->_order_by, $AsOrder)) {
                    throw new Exception('error');
                }
            }

            if (!$limit == NULL) {
                if (!$this->db->limit($limit, $pageNumber)) {
                    throw new Exception('error');
                }
            }

            // return as row or result set according to value of $single
            $Res = $this->db->get($this->_table_name);
            if (!is_object($Res)) {
                throw new Exception('error');
            }
            return $Row = $Res->$method();
        } catch (Exception $e) {
            log_message('ERROR', sprintf('%s : %s : DB transaction failed. Error no: %s, Error msg:%s, Last query: %s', __CLASS__, __FUNCTION__, isset($this->db->error()['code']) ? $this->db->error()['code'] : '', isset($this->db->error()['message']) ? $this->db->error()['message'] : '', print_r($this->db->last_query(), TRUE)));
            return FALSE;
        }
    }

    /**
     * Purpose of the function is to return a specific record/records based on the $where condition.
     * default $single value set to 'FALSE' so that function returns result set. when retrieving single
     * row data $single can be set to 'TRUE' which returns an ROW.
     *
     * @param  Integer  $where  Array() or content which mentioning the scope of filtering
     * @param  Boolean  $single construct to help get_by method, If 'TRUE' meaning return single row, ELSE return result set
     * @return row / all rows in the specific table
     */
    public function get_by($where, $single = FALSE, $AsOrder = "asc", $limit = NULL, $pageNumber = 0) {
        $this->db->where($where);
        $Res = $this->get(NULL, $single, $AsOrder, $limit, $pageNumber);
        return $Res;
    }

    /**
     * Both 'SAVE' and 'INSERT' operations been handled from this function.
     * logic behind is, if $id is set then it will UPDATE and if '$id' set to NULL it will INSERT
     *
     * @param  Array()  $data Array() of data.
     * @param  Integer  $id   Page ID
     * @return id value of the updated/inserted row
     */
    public function save($data, $id = NULL, $ignorAutoIDset = false, $keyFilter = true) {
        try {
            //if timestamp is TRUE, set Timestamp
            if ($this->_timestamps == TRUE) {
                $now = date('Y-m-d H:i:s');
                $id || $data['created'] = $now; // if ID is set leave, else set $data['created'] = $now
                $data['modified'] = $now;
            }
            //insert
            if ($id === NULL) {
                if (!$ignorAutoIDset) {
                    !isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
                }

                if (!$this->db->set($data)) {
                    throw new Exception('error');
                }
                if (!$this->db->insert($this->_table_name)) {
                    throw new Exception('error');
                }
                // fetch the ID of the newly inserted row
                $id = $this->db->insert_id();
            }
            //update
            else {
                if ($keyFilter == true) {
                    $filter = $this->_primary_filter; // filer the primary key
                    $id = $filter($id); // filer the primary key
                }

                if (!$this->db->set($data)) {
                    throw new Exception('error');
                }
                if (!$this->db->where($this->_primary_key, $id)) {
                    throw new Exception('error');
                }
                if (!$this->db->update($this->_table_name)) {
                    throw new Exception('error');
                }
            }
            return $id;
        } catch (Exception $e) {
            print "<pre>";
            print_r(sprintf('%s : %s : DB transaction failed. Error no: %s, Error msg:%s, Last query: %s', __CLASS__, __FUNCTION__, isset($this->db->error()['code']) ? $this->db->error()['code'] : '', isset($this->db->error()['message']) ? $this->db->error()['message'] : '', print_r($this->db->last_query(), TRUE)));
            print "</pre>";
            die();
            log_message('ERROR', sprintf('%s : %s : DB transaction failed. Error no: %s, Error msg:%s, Last query: %s', __CLASS__, __FUNCTION__, isset($this->db->error()['code']) ? $this->db->error()['code'] : '', isset($this->db->error()['message']) ? $this->db->error()['message'] : '', print_r($this->db->last_query(), TRUE)));
            return FALSE;
        }
    }

    /**
    * Purpose of the function is to return a specific record/records based on multiple values in a $where clause.
    * default $single value set to 'FALSE' so that function returns result set. when retrieving single
    * row data $single can be set to 'TRUE' which returns an ROW.
    *
    * @param  String  $columnName  Column name to be searched
    * @param  Array   $whereIn  Multiple values which mentioning the scope of filtering
    * @param  Boolean $single construct to help get_by method, If 'TRUE' meaning return single row, ELSE return result set
    * @return row / all rows in the specific table
    */
   public function get_in($columnName, $whereIn, $single = FALSE, $AsOrder = "asc", $limit = NULL, $pageNumber = 0) {
       $this->db->where_in($columnName, $whereIn);
       $Res = $this->get(NULL, $single, $AsOrder, $limit, $pageNumber);
       return $Res;
   }

    /**
     * Updating data row using differnt collom keys except 'id' column
     * @param  Array()  $data Array() of data.
     * @param  Array()  $where array of condition
     * @return id value of the updated/inserted row
     */
    public function update_by($data, $where) {
        try {
            if ($this->_timestamps == TRUE) {
                $now = date('Y-m-d H:i:s');
                $data['modified'] = $now;
            }
            if (!$this->db->set($data)) {
                throw new Exception('error');
            }
            if (!$this->db->where($where)) {
                throw new Exception('error');
            }
            if (!$this->db->update($this->_table_name)) {
                throw new Exception('error');
            }
            $Res = $this->db->affected_rows();
            return $Res;
        } catch (Exception $e) {
            print "<pre>";
            print_r(sprintf('%s : %s : DB transaction failed. Error no: %s, Error msg:%s, Last query: %s', __CLASS__, __FUNCTION__, isset($this->db->error()['code']) ? $this->db->error()['code'] : '', isset($this->db->error()['message']) ? $this->db->error()['message'] : '', print_r($this->db->last_query(), TRUE)));
            print "</pre>";
            die();
            log_message('ERROR', sprintf('%s : %s : DB transaction failed. Error no: %s, Error msg:%s, Last query: %s', __CLASS__, __FUNCTION__, isset($this->db->error()['code']) ? $this->db->error()['code'] : '', isset($this->db->error()['message']) ? $this->db->error()['message'] : '', print_r($this->db->last_query(), TRUE)));
            return FALSE;
        }
    }

    /**
     * Soft delete record by table primary key
     *
     * @param  Integer  $id   primary key of the table
     * @return false if $id is not set and otherwise NO returns
     */
    public function delete($id) {
        $filter = $this->_primary_filter;
        $id = $filter($id);

        if (!$id) {
            return FALSE;
        }
        $data = array(
            'deleted' => 1,
        );
        $this->save($data, $id);
    }

    /**
     * delete record by table primary key
     *
     * @param  Integer  $id   primary key of the table
     * @return false if $id is not set and otherwise NO returns
     */
    public function hardDelete($id) {
        $filter = $this->_primary_filter;
        $id = $filter($id);

        if (!$id) {
            return FALSE;
        }
        try {
            if (!$this->db->where($this->_primary_key, $id)) {
                throw new Exception('error');
            }
            if (!$this->db->limit(1)) {
                throw new Exception('error');
            }
            if (!$Res = $this->db->delete($this->_table_name)) {
                throw new Exception('error');
            }
            return $Res;
        } catch (Exception $e) {
            log_message('ERROR', sprintf('%s : %s : DB transaction failed. Error no: %s, Error msg:%s, Last query: %s', __CLASS__, __FUNCTION__, isset($this->db->error()['code']) ? $this->db->error()['code'] : '', isset($this->db->error()['message']) ? $this->db->error()['message'] : '', print_r($this->db->last_query(), TRUE)));
            return FALSE;
        }
    }

    /**
     * delete record by table primary key
     *
     * @param  Integer  $id   primary key of the table
     * @return false if $id is not set and otherwise NO returns
     */
    public function hardDeleteBy($where) {

        try {
            if (!$this->db->where($where)) {
                throw new Exception('error');
            }
            if (!$Res = $this->db->delete($this->_table_name)) {
                throw new Exception('error');
            }
            return $Res;
        } catch (Exception $e) {
            log_message('ERROR', sprintf('%s : %s : DB transaction failed. Error no: %s, Error msg:%s, Last query: %s', __CLASS__, __FUNCTION__, isset($this->db->error()['code']) ? $this->db->error()['code'] : '', isset($this->db->error()['message']) ? $this->db->error()['message'] : '', print_r($this->db->last_query(), TRUE)));
            return FALSE;
        }
    }

    /**
     * purpose of this function is to call webservice via curl 
     *
     * @param  String  $url         webservice url
     * @param  Array   $data        passing data to webservice 
     * @param  array   $apiKey      this key provide by owner of webservice for validation 
     * @param  String  $userName    Outh username   
     * @param  String  $password    Outh password
     * @return resultset
     */
    public function curlPost($url, $data = array()) {
        $Res = array();
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $this->config->item('outh_uname') . ":" . $this->config->item('outh_pwd'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 0);
            $Res = curl_exec($ch);
            $this->logMessage('error', 'curlPostInfo', print_r(curl_getinfo($ch), true));
        } catch (Exception $e) {
            $this->logMessage('error', 'curlPost', $e->getMessage());
        }
        return $Res;
    }

    /**
     * purpose of this function is to call webservice via curl 
     *
     * @param  String  $url         webservice url
     * @param  Array   $data        passing data to webservice 
     * @param  array   $apiKey      this key provide by owner of webservice for validation 
     * @param  String  $userName    Outh username   
     * @param  String  $password    Outh password
     * @return resultset
     */
    public function curlPost2($url, $data = array(), $apiKey = NULL, $userName = NULL, $password = NULL, $apiKeyName = 'APIKEY') {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERPWD, $userName . ":" . $password);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                $apiKeyName . ': ' . $apiKey,
            ));
            $data = curl_exec($ch);
            return $data;
        } catch (Exception $e) {
            $this->logMessage('error', 'curlPost', $e->getMessage());
        }
    }

    public function curlGet($url, $data = array()) {
        $Res = array();
        try {
            $parameters = '?';
            foreach ($data as $key => $value) {
                $parameters .= $key . '=' . $value . '&';
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url . $parameters);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_VERBOSE, 0);
            $Res = curl_exec($ch);
        } catch (Exception $e) {
            $this->logMessage('error', 'curlGet', $e->getMessage());
        }
        return $Res;
    }


    public function curlGet2($url, $data = array(), $apiKey = NULL, $userName = NULL, $password = NULL, $apiKeyName = 'APIKEY') {
        $Res = array();
        try {
            $parameters = '';
            if (!empty($data)) {
                $parameters = '?';
                foreach ($data as $key => $value) {
                    $parameters .= $key . '=' . $value . '&';
                }
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url . $parameters);
            if ($userName && $password) {
                curl_setopt($ch, CURLOPT_USERPWD, $userName . ":" . $password);
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_VERBOSE, 0);
            if ($apiKey) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    $apiKeyName . ': ' . $apiKey,
                ));
            }
            $Res = curl_exec($ch);
        } catch (Exception $e) {
            $this->logMessage('error', 'curlGet', $e->getMessage());
        }
        return $Res;
    }


    /**
     * purpose of this function is to log the error or something  
     *
     * @param  String  $level       define the log Ex : error,info
     * @param  String  $logTitle    log title is use to identify log
     * @param  String  $logMessage  log message
     * @return true
     */
    public function logMessage($level = 'error', $logTitle, $logMessage) {
        log_message($level, $logTitle . ' : ' . $logMessage);
    }

    public function encryptString($string=""){
        $encryptedString=$this->encrypt->encode($string);
        $encryptedString=str_replace(array('+', '/', '='), array('-', '_', '~'), $encryptedString);
        return $encryptedString;
    }

    public function decryptString($string=""){
        $dec_string=str_replace(array('-', '_', '~'), array('+', '/', '='), $string);
        $dec_string=$this->encrypt->decode($dec_string);
        return $dec_string;
    }

    public function structureArrayWithGivenKey($array, $key = 'id')
    {
        $retArray = [];

        foreach ($array as $id => $value) {
            if (!empty($value->$key)) {
                $retArray[$value->$key] = $value;
            }
        }

        return $retArray;
    }

}
