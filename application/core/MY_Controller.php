<?php
/**
 * -----------------------------IMPORTANT-------------------------------
 * Programmer should NOT change or add any code without having a better
 * understanding how MY_CONTROLLER and Its methods been used
 * ---------------------------------------------------------------------
 *
 * My_Controller will be used for all the CRUD operations in the system.
 *
 * All the other models should be extend form My_Model
 * Most of the common operations been written in the My_Model so that
 * programmer can easily call methods in My_Model Class for all most
 * all Database Communication and minimize the coding in their projects.
 *
 * @author     Dulitha Rajapaksha <dulithamahishka94@gmail.com>
 * @copyright  Dulitha Rajapaksha
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 1.0.0
 */
class MY_Controller extends CI_Controller
{
    protected $_currentClass;
    protected $_viewEndPoint;
    protected $_template;
    protected $_currentMethod;
    protected $_view;

    function __construct(){
        parent::__construct();
        $this->data['errors'] = array();
        $this->data['site_name'] = config_item('site_name');
        $this->data['current_class'] = $this->_currentClass = $this->router->class;
        $this->data['system_version'] = '';
        $this->_currentMethod = $this->router->method;
        $this->_template = 'themes/' . $this->config->item('template') . '/';
        $this->_viewEndPoint = $this->_template  . $this->_currentClass.'/';
        $this->_view = $this->_template  . $this->_currentClass.'/'.$this->_currentMethod;
    }
}
