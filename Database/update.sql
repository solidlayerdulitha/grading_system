-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2020 at 02:31 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `grading_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` char(50) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `password` char(250) DEFAULT NULL,
  `user_type` tinyint(4) DEFAULT NULL,
  `access_key` text,
  `timeout` datetime DEFAULT NULL,
  `enable` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `user_type`, `access_key`, `timeout`, `enable`, `deleted`, `created`, `modified`) VALUES
(8, 'Teacher', 'teacher@grading.com', '8370548edf4a30ca79cda9c1d938e3b824cf4dd3c41c0328e59223add79db454abd85649bf65eb8e3aee5ae0ea715f32e53b0f4520d7077a2c04b7f336b24b06', 1, NULL, NULL, 1, 0, '2019-04-12 10:24:46', '2019-10-01 11:33:00'),
(19, 'Dulitha Rajapaksha', 'dulitha@gmail.com', '8370548edf4a30ca79cda9c1d938e3b824cf4dd3c41c0328e59223add79db454abd85649bf65eb8e3aee5ae0ea715f32e53b0f4520d7077a2c04b7f336b24b06', 2, NULL, NULL, 1, 0, '2020-07-11 15:49:31', '2020-07-11 15:49:31'),
(20, 'Saman Kumara', 'samankumara@gmail.com', '8370548edf4a30ca79cda9c1d938e3b824cf4dd3c41c0328e59223add79db454abd85649bf65eb8e3aee5ae0ea715f32e53b0f4520d7077a2c04b7f336b24b06', 2, NULL, NULL, 1, 0, '2020-07-11 15:49:55', '2020-07-11 15:49:55'),
(21, 'Jagath Perera', 'jagathperera@gmail.com', '8370548edf4a30ca79cda9c1d938e3b824cf4dd3c41c0328e59223add79db454abd85649bf65eb8e3aee5ae0ea715f32e53b0f4520d7077a2c04b7f336b24b06', 2, NULL, NULL, 1, 0, '2020-07-11 15:50:14', '2020-07-11 15:50:14'),
(22, 'I am API', 'api@gmail.com', NULL, 3, '080d119956220bfef984e51e4768d87f03bb511e14ae84b8d9dc94cca2825f38196f09b0c36320d66d61fc37f3bfdefd3371100a39b944de93952bc629bbf985', '2020-07-13 14:05:24', 1, 0, '2020-07-12 05:45:57', '2020-07-12 14:05:24');

-- --------------------------------------------------------

--
-- Table structure for table `admin_log`
--

DROP TABLE IF EXISTS `admin_log`;
CREATE TABLE `admin_log` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT '0',
  `admin_name` char(50) DEFAULT '0',
  `controller` char(50) DEFAULT '0',
  `method` char(50) DEFAULT '0',
  `description` char(50) DEFAULT '0',
  `is_audit` char(50) DEFAULT '0',
  `old_value` text,
  `new_value` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_type`
--

DROP TABLE IF EXISTS `admin_type`;
CREATE TABLE `admin_type` (
  `id` int(11) NOT NULL,
  `admin_type` char(50) DEFAULT NULL,
  `machine_name` char(20) NOT NULL,
  `enable` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_type`
--

INSERT INTO `admin_type` (`id`, `admin_type`, `machine_name`, `enable`, `deleted`, `created`, `modified`) VALUES
(1, 'Teacher', 'TEACHER', 1, 0, NULL, NULL),
(2, 'Student', 'STUDENT', 1, 0, NULL, NULL),
(3, 'API User', 'API', 1, 0, '2020-07-12 00:00:00', '2020-07-12 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `answer_data`
--

DROP TABLE IF EXISTS `answer_data`;
CREATE TABLE `answer_data` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `result` char(20) NOT NULL,
  `result_data_id` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_data`
--

INSERT INTO `answer_data` (`id`, `student_id`, `question_id`, `answer`, `result`, `result_data_id`, `assignment_id`, `created`, `modified`) VALUES
(1, 1, 3, 'an electronic device for storing and processing data, typically in binary form, according to instructions given to it in a variable program.', 'RIGHT', 1, 1, '2020-07-11 16:55:01', '2020-07-11 16:55:01'),
(2, 1, 4, 'Programming is a way to “instruct the computer to perform various tasks”.', 'RIGHT', 1, 1, '2020-07-11 16:55:01', '2020-07-11 16:55:01'),
(3, 1, 5, 'I dont know', 'WRONG', 2, 3, '2020-07-11 16:55:01', '2020-07-11 16:55:01'),
(4, 1, 6, 'a rule defining correct procedure or behaviour in a sport.', 'PARTIAL', 2, 3, '2020-07-11 16:55:01', '2020-07-11 16:55:01'),
(5, 2, 3, 'A device maybe', 'WRONG', 3, 1, '2020-07-11 16:55:01', '2020-07-11 16:55:01'),
(6, 2, 4, 'Programming is a way to “instruct the computer to perform various tasks” - 2.', 'RIGHT', 3, 1, '2020-07-11 16:55:01', '2020-07-11 16:55:01'),
(7, 2, 1, 'Biology is the natural science that studies life and living organisms', 'RIGHT', 4, 2, '2020-07-11 16:55:02', '2020-07-11 16:55:02'),
(8, 2, 6, 'Animals can be insects, mammals, reptiles, fish, and other organisms that are not plants.', 'RIGHT', 4, 2, '2020-07-11 16:55:02', '2020-07-11 16:55:02');

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

DROP TABLE IF EXISTS `assignment`;
CREATE TABLE `assignment` (
  `id` int(11) NOT NULL,
  `assignment_name` char(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assignment`
--

INSERT INTO `assignment` (`id`, `assignment_name`, `status`, `created`, `modified`) VALUES
(1, 'Computer Programming', 1, '2020-07-11 15:46:54', '2020-07-11 15:46:54'),
(2, 'Biology', 1, '2020-07-11 15:47:02', '2020-07-11 15:47:02'),
(3, 'Cyber law', 1, '2020-07-11 15:47:12', '2020-07-11 15:47:12');

-- --------------------------------------------------------

--
-- Table structure for table `assignment_question`
--

DROP TABLE IF EXISTS `assignment_question`;
CREATE TABLE `assignment_question` (
  `id` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assignment_question`
--

INSERT INTO `assignment_question` (`id`, `assignment_id`, `question`, `status`, `created`, `modified`) VALUES
(1, 2, 'What is Biology?', 1, '2020-07-11 15:47:27', '2020-07-11 15:47:27'),
(2, 2, 'Who are animals?', 1, '2020-07-11 15:47:35', '2020-07-11 15:47:35'),
(3, 1, 'What is computer?', 1, '2020-07-11 15:47:51', '2020-07-11 15:47:51'),
(4, 1, 'What is programming?', 1, '2020-07-11 15:48:05', '2020-07-11 15:48:05'),
(5, 3, 'What is cyber space?', 1, '2020-07-11 15:48:18', '2020-07-11 15:48:18'),
(6, 3, 'What is Law?', 1, '2020-07-11 15:48:25', '2020-07-11 15:48:25');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `id` int(11) NOT NULL,
  `class_name` char(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `class_name`, `status`, `created`, `modified`) VALUES
(1, 'Maths Class', 1, '2020-07-09 00:00:00', '2020-07-09 00:00:00'),
(2, 'Science Class', 1, '2020-07-09 00:00:00', '2020-07-09 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `class_assignment`
--

DROP TABLE IF EXISTS `class_assignment`;
CREATE TABLE `class_assignment` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_name` char(255) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `assignment_name` char(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_assignment`
--

INSERT INTO `class_assignment` (`id`, `class_id`, `class_name`, `assignment_id`, `assignment_name`, `status`, `created`, `modified`) VALUES
(1, 1, 'Maths Class', 1, 'Computer Programming', 1, '2020-07-11 15:48:39', '2020-07-11 15:48:39'),
(2, 1, 'Maths Class', 3, 'Cyber law', 1, '2020-07-11 15:48:39', '2020-07-11 15:48:39'),
(3, 2, 'Science Class', 2, 'Biology', 1, '2020-07-11 15:48:43', '2020-07-11 15:48:43'),
(4, 2, 'Science Class', 1, 'Computer Programming', 1, '2020-07-11 15:48:54', '2020-07-11 15:48:54');

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

DROP TABLE IF EXISTS `keys`;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `method` char(120) NOT NULL,
  `params` text,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT '0',
  `message` text,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `results_data`
--

DROP TABLE IF EXISTS `results_data`;
CREATE TABLE `results_data` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `result` char(10) NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL,
  `attempt_number` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results_data`
--

INSERT INTO `results_data` (`id`, `student_id`, `assignment_id`, `result`, `time_start`, `time_end`, `attempt_number`, `created`, `modified`) VALUES
(1, 1, 1, '74', '2020-07-09 04:42:10', '2020-07-09 06:20:40', 1, '2020-07-11 16:55:01', '2020-07-11 16:55:01'),
(2, 1, 3, '22', '2020-07-09 07:00:22', '2020-07-09 08:10:30', 1, '2020-07-11 16:55:01', '2020-07-11 16:55:01'),
(3, 2, 1, '66', '2020-07-09 08:55:10', '2020-07-09 10:32:40', 1, '2020-07-11 16:55:01', '2020-07-11 16:55:01'),
(4, 2, 2, '57', '2020-07-09 06:00:22', '2020-07-09 08:59:30', 1, '2020-07-11 16:55:02', '2020-07-11 16:55:02');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `super_admin` tinyint(4) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `weight`, `status`, `super_admin`, `deleted`, `created`, `modified`) VALUES
(1, 'Super Admin', 0, 1, 1, 0, '2020-05-18 22:42:52', '2020-05-18 22:42:52'),
(2, 'Admin', 0, 1, 0, 0, '2020-05-18 22:42:52', '2020-05-18 22:42:52'),
(3, 'Editor', 0, 1, 0, 0, '2020-05-18 22:42:52', '2020-05-18 22:42:52');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `student_name` char(255) NOT NULL,
  `login_id` int(11) NOT NULL,
  `email` char(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `student_name`, `login_id`, `email`, `status`, `created`, `modified`) VALUES
(1, 'Dulitha Rajapaksha', 19, 'dulitha@gmail.com', 1, '2020-07-11 15:49:31', '2020-07-11 15:49:31'),
(2, 'Saman Kumara', 20, 'samankumara@gmail.com', 1, '2020-07-11 15:49:55', '2020-07-11 15:49:55'),
(3, 'Jagath Perera', 21, 'jagathperera@gmail.com', 1, '2020-07-11 15:50:14', '2020-07-11 15:50:14');

-- --------------------------------------------------------

--
-- Table structure for table `student_class`
--

DROP TABLE IF EXISTS `student_class`;
CREATE TABLE `student_class` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_name` char(255) NOT NULL,
  `student_id` int(11) NOT NULL,
  `student_name` char(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_class`
--

INSERT INTO `student_class` (`id`, `class_id`, `class_name`, `student_id`, `student_name`, `status`, `created`, `modified`) VALUES
(1, 1, 'Maths Class', 1, 'Dulitha Rajapaksha', 1, '2020-07-11 15:49:31', '2020-07-11 15:49:31'),
(2, 2, 'Science Class', 2, 'Saman Kumara', 1, '2020-07-11 15:49:55', '2020-07-11 15:49:55'),
(3, 1, 'Maths Class', 3, 'Jagath Perera', 1, '2020-07-11 15:50:14', '2020-07-11 15:50:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_log`
--
ALTER TABLE `admin_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_type`
--
ALTER TABLE `admin_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answer_data`
--
ALTER TABLE `answer_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignment`
--
ALTER TABLE `assignment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignment_question`
--
ALTER TABLE `assignment_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_assignment`
--
ALTER TABLE `class_assignment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `results_data`
--
ALTER TABLE `results_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_name` (`name`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_class`
--
ALTER TABLE `student_class`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `admin_log`
--
ALTER TABLE `admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_type`
--
ALTER TABLE `admin_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `answer_data`
--
ALTER TABLE `answer_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `assignment`
--
ALTER TABLE `assignment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `assignment_question`
--
ALTER TABLE `assignment_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `class_assignment`
--
ALTER TABLE `class_assignment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `results_data`
--
ALTER TABLE `results_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `student_class`
--
ALTER TABLE `student_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
