# READ ME
Welcome to the Grading System. This application is developed for test purposes only.

###  How do I setup the application
1. Clone the application
2. Go to application/config/development/config.php and change `$config['base_url']` value to your project public_html.
example: `$config['base_url'] = http://localhost/grading_system/public_html`
3. Go to application/config/development/database.php and change `$db['local']` array's values to your database values.
4. Go to Database/update.sql and import the databse into the given database in step 3.
5. Go to the application and login.

------------


#### Sample API URLs

**Request Header : Application-Key => xQvVXrVcaZh4diC8wQ5s**

Request to `{{base_url}}/Api/register` [POST] with given `Application-Key` as a header param and, `name` and `email` as post fields. And on successful request it will return a access_token which you can use for your requests. Please use this access token in a header parameter named `Access-Key` along with the `Application-Key` for the future requests. 

Please be noted that this access token will be **valid for 24 hours** and you will have to request a new token which is valid another 24 hours after previous one is expired.

To renew the old access_token, please request to `{{base_url}}/Api/register` [POST] again as you requested a new token. With `name` and `email` as post fields and, `Application-Key` and previous `Access-Key` as the header params.

Sample End Points : 

	Retrieve all assignments : {{base_url}}/Api/assignment [GET]

	Retrieve questions for assignments : {{base_url}}/Api/question?assignment={{assignment_id}} [GET]

Every API request will be stored in the `logs` table.

Note : There are two sample accounts for Teacher and Student. You can create multiple student accounts from the teachers portal, and the default student portals password for the newly created users would be **admin**. Please be noted that in this MVP application I have made some assumptions as there is no external server to get the answers from. So I created a sample response from the external server in application/controllers/Api.php answers(). That response array needs to be created manually for the students assigned details. I have already created sample response for the student Dulitha and please find login credentials below.

	Login URL : {{base_url}}/User/login

	Teachers portal
	Email : teacher@grading.com
	Password : admin

	Student Portal
	Email : dulitha@gmail.com
	Password : admin
