$(document).ready(function() {
    if ($('.login-box').length) {
        $( "#login-form" ).submit(function( event ) {
            $('.email-field-check').hide();
            $('.pw-field-check').hide();
            if ($('.login-email').val() == '' || $('.login-email').val() == null) {
                $('.email-field-check').show();
                event.preventDefault();
            }

            if ($('.login-pw').val() == '' || $('.login-pw').val() == null) {
                $('.pw-field-check').show();
                event.preventDefault();
            }
        });
    }

    if ($('.profile-index').length) {
        $('.review-answers').on('click', function(e){
            var result_id = $(this).attr('data-result');
            var student_id = $(this).attr('data-stu');

            $.ajax({
               type: "POST",
               async: false,
               url: base_url+'Profile/getResultsData',
               data: {
                   result_id: result_id,
                   student_id: student_id,
               },
               success: function(data) {
                    var result = $.parseJSON(data);

                    var html = '';

                    $.each(result['answers'], function( key, value ) {
                        html +='<tr>';
                        html += '<td>'+value.question+'</td>';
                        html += '<td>'+value.answer+'</td>';
                        html += '<td><span style="color:'+value.color+'">'+value.results+'</span></td>';
                        html +='</tr>';
                    });

                    $('#resultsModalLabel').html('Total Results: ' + result['assingment']['marks']);
                    $('#result-modal-body').html(html);
                    $('#resultsModal').modal('show'); 
               }
           });
        });
    }

    if ($('.assignment-student-view').length) {
        $('.student_view_assignments').on('click', function(e){
            var student_id = $(this).attr('data-stu');
            var assignment_id = $(this).attr('data-assignment');

            $.ajax({
               type: "POST",
               async: false,
               url: base_url+'Assignment/getResultsData',
               data: {
                   assignment_id: assignment_id,
                   student_id: student_id,
               },
               success: function(data) {
                    var result = $.parseJSON(data);

                    var html = '';

                    html +='<tr>';
                    html += '<td>'+result['results']['no_of_attempts']+'</td>';
                    html += '<td>'+result['results']['avg_time']+'</td>';
                    html += '<td>'+result['results']['avg_result']+'</td>';
                    html +='</tr>';

                    $('#resultsModalLabel').html('Average Results of : ' + result['student_name']);
                    $('#result-modal-body').html(html);
                    $('#resultsModal').modal('show'); 
               }
           });
        });
    }

    if ($('.teacher-index').length) {
        $('.overall_grading').on('click', function(e){
            var assignment_id = $(this).attr('data-assignment');

            $.ajax({
               type: "POST",
               async: false,
               url: base_url+'Teacher/getQuestionAnalysis',
               data: {
                   assignment_id: assignment_id,
               },
               success: function(data) {
                    var result = $.parseJSON(data);

                    var html = '';

                    $.each(result, function( key, value ) {
                        html +='<tr>';
                        html += '<td><a href="javascript:void(0)" class="question_stats" data-question="'+key+'" data-assignment="'+ assignment_id +'">'+value.question+'</a></td>';
                        html += '<td>'+value.RIGHT+'</td>';
                        html += '<td>'+value.WRONG+'</td>';
                        html += '<td>'+value.PARTIAL+'</td>';
                        html +='</tr>';
                    });

                    $('#result-modal-body').html(html);
                    $('#resultsModal').modal('show'); 
               }
           });
        });

        $(document).on('click', '.question_stats', function(e){
            var question_id = $(this).attr('data-question');
            var assignment_id = $(this).attr('data-assignment');

            $.ajax({
                type: "POST",
                async: false,
                url: base_url+'Teacher/getQuestionStats',
                data: {
                   question_id: question_id,
                   assignment_id: assignment_id,
                },
                success: function(data) {
                    var result = $.parseJSON(data);

                    var html = '';

                    $.each(result, function( key, value ) {
                        $.each(value, function( student_id, stat ) {
                            html +='<tr>';
                            html += '<td>'+stat.student_name+'</td>';
                            html += '<td>'+stat.answer+'</td>';
                            html += '<td><span style="color:'+stat.color+'">'+stat.result+'</span></td>';
                            html += '<td>'+stat.attempt+'</td>';
                            html +='</tr>';
                        });
                    });

                    $('#answer-modal-body').html(html);
                    $('#answerModal').modal('show');
                }
            });

        });
    }

    if ($('.assignment-index').length) {
        $('.assignment_view_questions').on('click', function(e){
            var assignment_id = $(this).attr('data-assignment');

            $.ajax({
               type: "POST",
               async: false,
               url: base_url+'Assignment/viewQuestions',
               data: {
                   assignment_id: assignment_id,
               },
               success: function(data) {
                    var result = $.parseJSON(data);

                    var html = '';
                    $.each(result['questions'], function( key, value ) {
                        html +='<tr>';
                        html += '<td>'+value.question+'</td>';
                        html +='</tr>';
                    });

                    $('#resultsModalLabel').html('Questions for assignment  : ' + result['assignment'].assignment_name);
                    $('#result-modal-body').html(html);
                    $('#resultsModal').modal('show');
               }
           });
        });
    }

});